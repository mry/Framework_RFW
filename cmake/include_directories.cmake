

#------------------------------------------------------------------------------
# set include directory for utils
SET(RFW_UTILS_INCLUDE_DIR  ${PROJECT_SOURCE_DIR}/utils/include)
SET(RFW_UTILS_SOURCE_DIR   ${PROJECT_SOURCE_DIR}/utils/src)

# set include directory for periphery
SET(RFW_PERIPHERY_INCLUDE_DIR  ${PROJECT_SOURCE_DIR}/periphery/include)
SET(RFW_PERIPHERY_SOURCE_DIR   ${PROJECT_SOURCE_DIR}/periphery/src)

# set include directory for pattern
SET(RFW_PATTERN_INCLUDE_DIR  ${PROJECT_SOURCE_DIR}/pattern/include)
SET(RFW_PATTERN_SOURCE_DIR   ${PROJECT_SOURCE_DIR}/pattern/src)

# set include directory for statemachine
SET(RFW_STATEMACHINE_INCLUDE_DIR  ${PROJECT_SOURCE_DIR}/statemachine/include)
SET(RFW_STATEMACHINE_SOURCE_DIR   ${PROJECT_SOURCE_DIR}/statemachine/src)

#SET(RFW_DEPLOY_DIR  ${PROJECT_SOURCE_DIR}/deploy)

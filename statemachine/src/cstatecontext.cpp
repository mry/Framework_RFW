/***************************************************************************
    begin                : 2016
    copyright            : (C) 2016 by mry
    email                : mry@hispeed.ch
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#include "rfw/statemachine/istatemachinedata.h"
#include "rfw/statemachine/cbasestm.h"
#include "rfw/statemachine/cstatecontext.h"
#include "rfw/statemachine/cstatetransitions.h"

#include "rfw/utils/clogger.h"
#include "rfw/utils/cthread.h"

#include <algorithm>
#include <cassert>
#include <iostream>
#include <string.h>

namespace statemachine
{
/***********************************************************************//**
  @method :  CStateConext
  @comment:
  @param  :  threadId thread identifier
  @param  :  pThread pointer to main thread
***************************************************************************/
CStateContext::CStateContext(uint32_t threadId, utils::CThread * const pThread)
: m_cClockTimer(this)
, m_nThreadId{threadId}
, m_pThread{pThread}
, m_pStateTransitions{nullptr}
, m_pStateMachineData{nullptr}
{
}

/***********************************************************************//**
  @method :  ~CStateContext
  @comment:
***************************************************************************/
CStateContext::~CStateContext()
{
  baseStmMap_t::iterator iter;
  for (iter = m_mapStateMap.begin(); iter != m_mapStateMap.end();++iter){
    CBaseStm* pState = (*iter).second;
    utils::SafeDelete(pState);
  }
  INFO("~CStateContext");
}

/***********************************************************************//**
  @method :  SetStartState
  @comment:  set the initial state
  @param  :  startState the start state
  @return :  -
***************************************************************************/
void CStateContext::SetStartState(uint32_t startState)
{
  // Evaluate deepest state
  CBaseStm* pStartState = EvaluateNextState(startState);
  assert(pStartState!=nullptr);
  m_nActiveState = pStartState->GetStateId();

  std::vector<uint32_t>ActiveStateTree = GetStateTree(pStartState);

  std::vector<uint32_t>::reverse_iterator itNext;
  for (itNext = ActiveStateTree.rbegin(); itNext != ActiveStateTree.rend(); ++itNext){
    CBaseStm* pState = GetState(*itNext);
    if (pState){
      pState->OnEntry();
    }else{
      ERR ("state does not exist");
    }
  }
}

/***********************************************************************//**
  @method :  SetTransitions
  @comment:  set transitions class
  @param  :  pTransitions pointer to transition
  @return :  -
***************************************************************************/
void CStateContext::SetTransitions(IStateTransitions * pTransitions)
{
  m_pStateTransitions = pTransitions;
}

/***********************************************************************//**
  @method :  SetStateMachineData
  @comment:  set data for state machine
  @param  :  pStateMachineData pointer to state machine
  @return :  -
***************************************************************************/
void CStateContext::SetStateMachineData(IStateMachineData * pStateMachineData)
{
  m_pStateMachineData = pStateMachineData;
}

/***********************************************************************//**
  @method :  GetStateMachineData
  @comment:  get state machine data
  @return :  pointer const to state machine data
***************************************************************************/
IStateMachineData * const CStateContext::GetStateMachineData()
{
  return m_pStateMachineData;
}

/***********************************************************************//**
  @method :  AddState
  @comment:  Add state to state machine
  @param  :  pState the state
  @return : -
***************************************************************************/
void CStateContext::AddState(CBaseStm* pState)
{
  uint32_t stateId = pState->GetStateId();
  m_mapStateMap.insert(std::pair<uint32_t, CBaseStm*>(stateId, pState));
}

/***********************************************************************//**
  @method :  SetNextState
  @comment:  set the next state
  @param  :  invokingState the state where the state transition was invoked
  @param  :  nextState state id of next state
  @return :  -
***************************************************************************/
void CStateContext::SetNextState(uint32_t invokingState, 
                                 uint32_t nextState)
{
  // Evaluate next state since next state might not point
  // to deepest nested state
  CBaseStm* pNextState = EvaluateNextState(nextState);
  assert(pNextState!=nullptr);

  CBaseStm* pActiveState = GetActiveState();
  std::vector<uint32_t>ActiveStateTree = GetStateTree(pActiveState);
  std::vector<uint32_t>NextStateTree   = GetStateTree(pNextState);

  uint32_t nextStartPoint = ProcessOnExit(ActiveStateTree,NextStateTree);

  /// transition call with input parameters invoking state and next state
  if (m_pStateTransitions){
    m_pStateTransitions->CallFunction(invokingState,nextState);
  }

  ProcessOnEntry(NextStateTree,nextStartPoint);

  // Assign next state
  m_nActiveState = pNextState->GetStateId();
}

/***********************************************************************//**
  @method :  ProcessOnExit
  @comment:  Process exit state. Check lca with new state and call OnExit.
  @param  :  ActiveStateTree vector of state hierarchy of active state
  @param  :  NextStateTree vector of state hierarchy of next state
  @return :  nextStartPoint
***************************************************************************/
uint32_t CStateContext::ProcessOnExit (
    std::vector<uint32_t> &ActiveStateTree,
    std::vector<uint32_t> &NextStateTree)
{
  uint32_t nextStartPoint = 0;

  std::vector<uint32_t>::iterator itActive = ActiveStateTree.begin();
  bool bFound = false;
  while (itActive != ActiveStateTree.end() && !bFound) {
    // Check if node id is also part of next state tree
    std::vector<uint32_t>::iterator lca = find(NextStateTree.begin(), NextStateTree.end(), (*itActive));
    if (lca != NextStateTree.end()) {
      // Element was found
      bFound = true;
      nextStartPoint = (*lca);
    } else {
      CBaseStm* pState = GetState(*itActive);
      assert(pState!=nullptr);
      pState->OnExit();
      pState->StopTimer();
      UpdateHistoryChild(pState);
      itActive++;
    }
  }
  return nextStartPoint;
}

/***********************************************************************//**
  @method :  UpdateHistoryChild
  @comment:  Update the history state of the parent state, if any
  @param  :  pState state to process
  @return :  -
***************************************************************************/
void CStateContext::UpdateHistoryChild(CBaseStm* const pState)
{
  CBaseStm* const parent = pState->GetParent();
  if (parent) {
    parent->SetHistoryChild(pState);
  }
}

/***********************************************************************//**
  @method :  ProcessOnEntry
  @comment:  Process entry state. Check call OnEntry of according states
  @param  :  NextStateTree vector of state hierarchy of next state
  @param  :  nextStartPoint identifier of next start point
  @return :  -
***************************************************************************/
void CStateContext::ProcessOnEntry(
    std::vector<uint32_t> &NextStateTree,
    uint32_t nextStartPoint)
{
  std::vector<uint32_t>::reverse_iterator itNext = find(NextStateTree.rbegin(),
                                                        NextStateTree.rend(),
                                                        nextStartPoint);
  itNext++;
  while (itNext != NextStateTree.rend()){
    CBaseStm* pState = GetState(*itNext);
    if (pState){
      pState->OnEntry();
    }else{
      ERR("ERROR No State!!!");
    }
    itNext++;
  }
}

/***********************************************************************//**
  @method : GetActiveState
  @comment: get the active state
  @return : get the pointer to the active state
***************************************************************************/
CBaseStm* CStateContext::GetActiveState()
{
  return GetState(m_nActiveState);
}

/***********************************************************************//**
  @method : GetState
  @comment: get the state instance according to the identifier
  @param  : nextState identifier
  @return : get the pointer to the state instance
        The pointer can be NULL!
***************************************************************************/
CBaseStm* const CStateContext::GetState(uint32_t nextState)
{
  // get state from the state list
  CBaseStm * pState{nullptr};
  baseStmMap_t::iterator iter = m_mapStateMap.find(nextState);
  if( iter != m_mapStateMap.end() ) {
    pState = (*iter).second;
  }
  return pState;
}

/***********************************************************************//**
  @method : EvaluateNextState
  @comment: get deepest state in a state
  @param  : nextState identifier
  @return : get the pointer to the state instance
        The pointer can be NULL!
***************************************************************************/
CBaseStm* const CStateContext::EvaluateNextState(uint32_t nextState)
{
  CBaseStm* pState = GetState(nextState);

  // if state exist, get child at the very bottom
  if (pState) {
    // find substate
    while (pState->GetChild()) {
      pState = pState->GetChild();
    }
  }
  return pState;
}

/***********************************************************************//**
  @method :  GetStateTree
  @comment:  get the state tree up to the root item
  @param  :  pState the instance of the state
  @return :  vector containing the tree. First element is pState
***************************************************************************/
std::vector<uint32_t> CStateContext::GetStateTree(CBaseStm*  pState)
{
  std::vector <uint32_t> stateTree;
  CBaseStm* pLocalState = pState;
  while (pLocalState) {
    stateTree.push_back(pLocalState->GetStateId());
    pLocalState = pLocalState->GetParent();
  }
  return stateTree;
}

/***********************************************************************//**
  @method : Event
  @comment: send an event to the state machine
  @param  : pMessage event message
  @return : -
***************************************************************************/
void CStateContext::Event(SMART_PTR(utils::CMessage) pMsg)
{
  std::unique_lock<std::mutex> lock(m_sMutex);
  CBaseStm* pActiveState = GetActiveState();
  std::vector<uint32_t>ActiveStateTree = GetStateTree(pActiveState);
  std::vector<uint32_t>::iterator itActive = ActiveStateTree.begin();

  bool bProcessed = false;
  while (itActive!=ActiveStateTree.end() && !bProcessed){
    CBaseStm* pState = GetState(*itActive);
    if (pState){
      bProcessed = pState->OnEvent(pMsg);
    }else{
      ERR("ERROR No State!!!");
    }
    itActive++;
  }
}

/***********************************************************************//**
  @method : TimerEvent
  @comment: emit timer event, override method if own timer handling needed.
  @return : -
***************************************************************************/
void CStateContext::TimerEvent()
{
  SMART_PTR(utils::CEventMessage) pMsg = utils::CREATE_SMART_PTR(utils::CEventMessage);
  pMsg->SetCommand(EN_CMD_TIMEOUT);
  m_pThread->TransmitMessage(pMsg);
}

/***********************************************************************//**
  @method : StartTimer
  @comment: start a timer with timeout time
  @param  : nTimeInMilliSec time in milliseconds
  @return : -
***************************************************************************/
void CStateContext::StartTimer(uint64_t nTimeInMilliSec)
{
  m_cClockTimer.StartTimer(nTimeInMilliSec);
}

/***********************************************************************//**
  @method : StopTimer
  @comment: stop timer event
  @return : -
***************************************************************************/
void CStateContext::StopTimer()
{
  m_cClockTimer.StopTimer();
}

};
/* END_MODULE */

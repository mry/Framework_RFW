/***************************************************************************
    begin                : 2016
    copyright            : (C) 2016 by mry
    email                : mry@hispeed.ch
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#include "rfw/statemachine/cbasestm.h"
#include "rfw/statemachine/cstatecontext.h"

namespace statemachine
{

/***********************************************************************//**
  @method : CBaseStm
  @comment: constructor
  @param  : pContext pointer to the main context of the state machine
  @param  : nStateId  identifier of state
  @param  : bHistory enable history for state
***************************************************************************/
CBaseStm::CBaseStm(IStateContext * const pContext, 
                   uint32_t nStateId, 
                   bool bHistory)
  : m_nStateId{nStateId}
  , m_pParent{nullptr}
  , m_pChild{nullptr}
  , m_pStateContext{pContext}
  , m_bHistoryEnabled{bHistory}
  , m_bTimerStarted{false}
{
}

/***********************************************************************//**
  @method :  ~CBaseStm
  @comment:  destructor
***************************************************************************/
CBaseStm::~CBaseStm()
{
}

/***********************************************************************//**
  @method : SetParent
  @comment: set the parent state of this state
  @param  : parent pointer to parent state
  @return : -
***************************************************************************/
void CBaseStm::SetParent(CBaseStm* parent)
{
  m_pParent = parent;
}

/***********************************************************************//**
  @method : GetStateContext
  @comment: get the state context
  @return : state context
***************************************************************************/
IStateContext* CBaseStm::GetStateContext() const
{
    return m_pStateContext;
}

/***********************************************************************//**
  @method : GetParent
  @comment: get the parent state of this state
  @return : pointer to parent or null
***************************************************************************/
CBaseStm* CBaseStm::GetParent()
{
  return m_pParent;
}

/***********************************************************************//**
  @method : SetChild
  @comment: set the child state of this state
  @param  : child pointer to child
  @return : -
***************************************************************************/
void CBaseStm::SetChild(CBaseStm* child)
{
  m_pChild = child;
}

/***********************************************************************//**
  @method : SetHistoryEnabled
  @comment: set history enabled or disabled
  @param  : bHistoryEnabled history behaviour enabled or disabled
  @return : -
***************************************************************************/
void CBaseStm::SetHistoryEnabled(bool bHistoryEnabled)
{
  m_bHistoryEnabled = bHistoryEnabled;
}

/***********************************************************************//**
  @method : SetHistoryChild
  @comment: get the parent state of this state
  @param  : history history child
  @return : -
***************************************************************************/
void CBaseStm::SetHistoryChild(CBaseStm* history)
{
  if (m_bHistoryEnabled){
    m_pChild = history;
  }
}

/***********************************************************************//**
  @method : GetChild
  @comment: get the child state of this state
  @return : pointer to parent or null
***************************************************************************/
CBaseStm * CBaseStm::GetChild()
{
  return m_pChild;
}

/***********************************************************************//**
  @method : GetStateId
  @comment: get the identifier of this state
  @return : identifier of this state
***************************************************************************/
uint32_t CBaseStm::GetStateId()
{
  return m_nStateId;
}

/***********************************************************************//**
  @method : ChangeState
  @comment: convenience operation to change state
  @param  : nState state id of next state
  @return : -
***************************************************************************/
void CBaseStm::ChangeState(uint32_t nState)
{
  m_pStateContext->SetNextState(m_nStateId, nState);
}

/***********************************************************************//**
  @method : StartTimer
  @comment: start a timer with timeout time
  @param  : nTimeInMilliSecs time in milliseconds
  @return : -
***************************************************************************/
void CBaseStm::StartTimer(uint64_t nTimeInMilliSecs)
{
  m_pStateContext->StartTimer(nTimeInMilliSecs);
  m_bTimerStarted = true;
}

/***********************************************************************//**
  @method : StopTimer
  @comment: stop timer event
  @return : -
***************************************************************************/
void CBaseStm::StopTimer()
{
  if (m_bTimerStarted){
    m_pStateContext->StopTimer();
  }
  m_bTimerStarted = false;
}
}
/* END_MODULE */

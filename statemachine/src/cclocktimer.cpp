/***************************************************************************
    begin                : 2016
    copyright            : (C) 2016 by mry
    email                : mry@hispeed.ch
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#include "rfw/statemachine/cclocktimer.h"
#include "rfw/utils/clogger.h"

#include <cassert>
#include <iostream>
#include <signal.h>
#include <sstream>
#include <string.h>
#include <time.h>

namespace statemachine
{

/***********************************************************************//**
  @method :  CClockTimer
  @comment:  constructor
  @param:    pStateContext state context to call
***************************************************************************/
CClockTimer::CClockTimer(IStateContext* pStateContext)
  : m_pStateContext(pStateContext)
  , m_bTimerRunning(false)
  , m_ioservice()
  , m_timer(m_ioservice)
{
}

/***********************************************************************//**
  @method :  ~CClockTimer
  @comment:  destructor
***************************************************************************/
CClockTimer::~CClockTimer()
{
}

/***********************************************************************//**
  @method :  StartTimer
  @comment:  start the timer
  @param  :  nTimeInMillisecs interval in milliseconds
  @return :  -
***************************************************************************/
void CClockTimer::StartTimer(uint64_t nTimeInMillisecs)
{
  if (!m_bTimerRunning) {
    m_runner = std::thread([&,nTimeInMillisecs](){
      m_bTimerRunning = true;
      m_timer.expires_from_now(boost::posix_time::milliseconds(nTimeInMillisecs));
      m_timer.async_wait(bind(&CClockTimer::SignalHandler, this, _1));
      m_ioservice.reset();
      m_ioservice.run();  // blocking
    });
    
  } else {
    ERR("Timer already started");
  }
}

/***********************************************************************//**
  @method :  StopTimer
  @comment:  stop the timer
  @return :  -
***************************************************************************/
void CClockTimer::StopTimer()
{
  /// stop the timer
  m_bTimerRunning = false;
  m_timer.cancel();
  if (m_runner.joinable()) {
    m_runner.join();
  }
}

/***********************************************************************//**
  @method :  SignalHander
  @comment:  Signal handler for timeout
  @param  :  error signal id
  @return :  -
***************************************************************************/
void CClockTimer::SignalHandler(const boost::system::error_code& error) 
{
  if (error == boost::asio::error::operation_aborted){
    WARN("Timer Aborted");
    m_bTimerRunning = false;
  } else if (error) {
    ERR("Timer error");
  } else {
    CallTimerEvent();
  }
}

/***********************************************************************//**
  @method :  CallTimerEvent
  @comment:  operation called to invoke timer event
  @return :  -
***************************************************************************/
void CClockTimer::CallTimerEvent()
{
  m_bTimerRunning = false;
  if (m_pStateContext) {
    m_pStateContext->TimerEvent();
  } else {
    ERR("m_pStateContext is NULL");
  }
}
}
/* END_MODULE */

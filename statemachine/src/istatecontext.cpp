/***************************************************************************
    begin                : 2016
    copyright            : (C) 2016 by mry
    email                : mry@hispeed.ch
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#include "rfw/statemachine/istatecontext.h"

namespace statemachine
{
/***********************************************************************//**
  @method :  IStateContext
  @comment:  constructor
***************************************************************************/
IStateContext::IStateContext()
{
}

/***********************************************************************//**
  @method :  ~IStateContext
  @comment:  destructor
***************************************************************************/
IStateContext::~IStateContext()
{
}
}
/* END_MODULE */

/***************************************************************************
    begin                : 2016
    copyright            : (C) 2016 by mry
    email                : mry@hispeed.ch
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#include "rfw/statemachine/cstatetransitions.h"
#include "rfw/utils/clogger.h"

namespace statemachine
{
/***********************************************************************//**
  @method :  CStateTransitions
  @comment:  destructor
***************************************************************************/
CStateTransitions::~CStateTransitions()
{
  m_Functions.clear();
}

/***********************************************************************//**
  @method :  SetStateContext
  @comment:  set pointer to state context
  @param  :  pStateContext pointer to state context
  @return :  -
***************************************************************************/
void CStateTransitions::SetStateContext(IStateContext * pStateContext)
{
  m_pStateContext = pStateContext;
}

/***********************************************************************//**
  @method :  CreateHash
  @comment:  compute a hash value for two input parameters
  @param  :  in1 parameter1
  @param  :  in2 parameter2
  @return :  hash value
***************************************************************************/
uint32_t CStateTransitions::CreateHash(uint32_t in1, 
                                       uint32_t in2)
{
  return ((in1 >> 10) + in2);
}

/***********************************************************************//**
  @method :  AddFunction
  @comment:  add a function pointer to the map
  @param  :  source source state id
  @param  :  target target state id
  @param  :  memberFnc member function to add
  @return :  -
***************************************************************************/
void CStateTransitions::AddFunction(uint32_t source, 
                                    uint32_t target, 
                                    pt2Member memberFnc)
{
  m_Functions.insert(CFunctionMap_t::value_type(CreateHash(source,target),memberFnc));
}

/***********************************************************************//**
  @method :  CreateStateTransitions
  @comment:  create state transition table. This is for demo purpose
             in order to get the idea how to add a function.
  @return :  -
***************************************************************************/
//void CStateTransitions::CreateStateTransitions()
//{
//    AddFunction(source,target,std::bind(&MyDerivedClass::MyOperation,this);
//    ... more ...
//}


/***********************************************************************//**
  @method :  CallFunction
  @comment:  call a transition function
  @param  :  source source state id
  @param  :  target target state id
  @return :  -
***************************************************************************/
void CStateTransitions::CallFunction(uint32_t source, 
                                     uint32_t target)
{
  CFunctionMapIterator iter;
  iter = m_Functions.find(CreateHash(source,target));
  if (iter == m_Functions.end()){
    ERR("No Transition found");
  } else {
    pt2Member func = (*iter).second;
    func();
  }
}
}
/* END_MODULE */

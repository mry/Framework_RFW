/***************************************************************************
    begin                : 2016
    copyright            : (C) 2016 by mry
    email                : mry@hispeed.ch
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#include "rfw/statemachine/istatemachinedata.h"

namespace statemachine
{
/***********************************************************************//**
  @method :  IStateMachineData
  @comment:  constructor
***************************************************************************/
IStateMachineData::IStateMachineData()
{
}

/***********************************************************************//**
  @method :  ~IStateMachineData
  @comment:  destructor
***************************************************************************/
IStateMachineData::~IStateMachineData()
{
}
}
/* END_MODULE */

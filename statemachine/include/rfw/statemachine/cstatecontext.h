/***************************************************************************
    begin                : 2016
    copyright            : (C) 2016 by mry
    email                : mry@hispeed.ch
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#ifndef CSTATECONTEXT_H
#define CSTATECONTEXT_H

#include "rfw/statemachine/istatecontext.h"
#include "rfw/statemachine/cclocktimer.h"

#include "rfw/utils/ctypes.h"
#include "rfw/utils/cmessage.h"
#include "rfw/utils/ctimermessage.h"

#include <csignal>
#include <map>
#include <vector>
#include <mutex>

namespace utils
{
class CThread;
};

namespace statemachine
{
class CBaseStm;
class IStateTransitions;
class IStateMachineData;

/***********************************************************************//**
  @class  : CStateContext
  @package: Utils
***************************************************************************/
class CStateContext 
: public IStateContext
{
  /// this class can not be copied
  UNCOPYABLE(CStateContext);

public :
  typedef enum
  {
    EN_INVAL = 0,
    EN_FIRST = 1
  } enBaseState;

  typedef enum
  {
    EN_CMD_INVAL   = 0,
    EN_CMD_TIMEOUT = 1,
    EN_CMD_FIRST   = 2
  } enBaseCommand;

public:
  explicit CStateContext(uint32_t threadId, utils::CThread* const pThread);
  virtual ~CStateContext();

  virtual void SetNextState(uint32_t invokingState, uint32_t nextState);

  CBaseStm* GetActiveState();
  void SetStartState(uint32_t startState);
  void SetTransitions(IStateTransitions * pTransitions);

  virtual void Event(SMART_PTR(utils::CMessage) pMsg);
  virtual void CreateStateMachine() = 0;
  virtual void TimerEvent();

  virtual void StartTimer(uint64_t nTimeInMilliSec);
  virtual void StopTimer();

  virtual void SetStateMachineData(IStateMachineData * pStateMachineData);
  virtual IStateMachineData * const GetStateMachineData();

protected:
  CBaseStm* const GetState(uint32_t nextState);
  
  CBaseStm* const EvaluateNextState(uint32_t nextState);
  
  void AddState(CBaseStm* const pState);
  
  std::vector<uint32_t> GetStateTree(CBaseStm* pState);
  
  uint32_t ProcessOnExit (
    std::vector<uint32_t> &ActiveStateTree,
    std::vector<uint32_t> &NextStateTree);

  void ProcessOnEntry(
    std::vector<uint32_t> &NextStateTree,
    uint32_t nextStartPoint);

private:
  void UpdateHistoryChild(CBaseStm* const pState);

  /// mutex internal use
  std::mutex  m_sMutex;

  /// timer used for timeouts
  CClockTimer m_cClockTimer;

  /// thread identifier
  uint32_t m_nThreadId;

  /// thread context
  utils::CThread * const m_pThread;

  /// state machine states
  typedef std::map <uint32_t, CBaseStm*> baseStmMap_t;

  /// state map of all states
  std::map <uint32_t, CBaseStm*> m_mapStateMap;

  /// active state
  uint32_t m_nActiveState;

  /// state transitions
  IStateTransitions * m_pStateTransitions;

  /// state machine data
  IStateMachineData * m_pStateMachineData;
};
};
#endif
/* CSTATECONTEXT_H */

/***************************************************************************
    begin                : 2016
    copyright            : (C) 2016 by mry
    email                : mry@hispeed.ch
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#ifndef CSTATETRANSITIONS_H_
#define CSTATETRANSITIONS_H_

/***********************************************************************//**
  @class  : CStateTransitions
  @package: Utils
***************************************************************************/
#include "rfw/statemachine/istatetransitions.h"
#include "rfw/statemachine/cstatecontext.h"

namespace statemachine
{

/// definition for function map
typedef std::map<uint32_t, pt2Member> CFunctionMap_t;

/// definition for function map iterator
typedef std::map<uint32_t, pt2Member>::iterator CFunctionMapIterator;

class CStateTransitions 
: public IStateTransitions
{
  UNCOPYABLE(CStateTransitions);

public:
  CStateTransitions() = default;
  virtual ~CStateTransitions();
  void CallFunction(uint32_t source, uint32_t target) override;
  void SetStateContext(IStateContext *  pStateContext) override;

  virtual void CreateStateTransitions() = 0;

protected:
  virtual void AddFunction(uint32_t source, 
                           uint32_t target, 
                           pt2Member memberFnc);

private:
  uint32_t CreateHash(uint32_t in1, uint32_t in2);
  IStateContext * m_pStateContext;
  std::map <uint32_t ,pt2Member> m_Functions;
};
};
#endif
/* CSTATETRANSITIONS_H_ */

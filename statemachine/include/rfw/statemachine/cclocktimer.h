/***************************************************************************
    begin                : 2016
    copyright            : (C) 2016 by mry
    email                : mry@hispeed.ch
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#ifndef CCLOCKTIMER_H_
#define CCLOCKTIMER_H_

#include "rfw/utils/ctypes.h"
#include "rfw/statemachine/istatecontext.h"

#include <boost/asio.hpp>
#include <boost/bind.hpp>
#include <boost/date_time/posix_time/posix_time.hpp>

#include <thread>

namespace statemachine
{

class IStateContext;

/***********************************************************************//**
  @class  : CClockTimer
  @package: Utils
***************************************************************************/
class CClockTimer
{
  /// this class can not be copied
  UNCOPYABLE( CClockTimer);
  
public:
  explicit CClockTimer(IStateContext* pStateContext);
  ~CClockTimer();
  void StartTimer(uint64_t nTimeInMillisecs);
  void StopTimer();

private:
  void SignalHandler(const boost::system::error_code& error);
  void CallTimerEvent();

private:
  IStateContext* m_pStateContext;
  bool m_bTimerRunning;
  std::thread m_runner;
  boost::asio::io_service m_ioservice;
  boost::asio::deadline_timer m_timer;
};
};
#endif
/* CClockTimer_H_ */

/***************************************************************************
    begin                : 2016
    copyright            : (C) 2016 by mry
    email                : mry@hispeed.ch
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#ifndef ISTATEMACHINEDATA_H_
#define ISTATEMACHINEDATA_H_

#include "rfw/utils/ctypes.h"

namespace statemachine
{

/***********************************************************************//**
  @class  : IStateMachineData
  @package: Utils
***************************************************************************/
class IStateMachineData
{
  /// this class can not be copied
  UNCOPYABLE(IStateMachineData);

public:
  IStateMachineData();
  virtual ~IStateMachineData();
};
};
#endif
/* ISTATEMACHINEDATA_H_ */

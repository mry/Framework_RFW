/***************************************************************************
    begin                : 2016
    copyright            : (C) 2016 by mry
    email                : mry@hispeed.ch
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#ifndef CBASESTM_H
#define CBASESTM_H

#include "rfw/utils/ctypes.h"
#include "rfw/utils/cmessage.h"
#include "rfw/statemachine/istatecontext.h"

namespace statemachine
{

/***********************************************************************//**
  @class  : CBaseStm
  @package: Utils
***************************************************************************/
class CBaseStm
{
  /// this class can not be copied
  UNCOPYABLE(CBaseStm);
  
public:
  explicit CBaseStm(IStateContext * const pContext, uint32_t nStateId, bool bHistory);
  virtual ~CBaseStm();

  void SetChild(CBaseStm* child);
  void SetHistoryEnabled(bool bHistoryEnabled);
  void SetHistoryChild(CBaseStm* history);
  void SetParent(CBaseStm* parent);

  CBaseStm* GetParent();
  CBaseStm* GetChild();

  uint32_t GetStateId();

/***********************************************************************//**
  @method :  OnEntry
  @comment:  pure virtual operation. Called when state is entered
  @return :  -
***************************************************************************/
  virtual void OnEntry() = 0;

/***********************************************************************//**
  @method :  OnExit
  @comment:  pure virtual operation. Called when state is exited
  @return :  -
***************************************************************************/
  virtual void OnExit()  = 0;

/***********************************************************************//**
  @method :  OnEvent
  @comment:  pure virtual operation. Called when an event occurs
  @param  :  pMsg parameter containing a message
  @return :  true:  Event is processed,
             false: Event will be processed in super state
***************************************************************************/
  virtual bool OnEvent(SMART_PTR(utils::CMessage) pMsg) = 0;

  void ChangeState(uint32_t nState);
  void StartTimer(uint64_t nTimeInMilliSecs);
  void StopTimer();

protected:
   IStateContext* GetStateContext() const;

private:
  uint32_t m_nStateId;
  CBaseStm* m_pParent;
  CBaseStm* m_pChild;
  IStateContext* const m_pStateContext;
  bool m_bHistoryEnabled;
  bool m_bTimerStarted;
};
};
#endif
/* CBASESTM_H */

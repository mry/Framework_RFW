/***************************************************************************
    begin                : 2016
    copyright            : (C) 2016 by mry
    email                : mry@hispeed.ch
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#ifndef ISTATETRANSITIONS_H_
#define ISTATETRANSITIONS_H_

#include "rfw/utils/ctypes.h"

#include <functional>

namespace statemachine
{

/// Forward declaration
class IStateContext;

/***********************************************************************//**
  @class  : IStateTransitions
  @package: Utils
***************************************************************************/

/// definition for pt2Member
typedef std::function <void(void)> pt2Member;

class IStateTransitions
{
  UNCOPYABLE(IStateTransitions);

public:
  IStateTransitions();
  virtual ~IStateTransitions();
  virtual void CreateStateTransitions() = 0;
  virtual void CallFunction(uint32_t source, uint32_t target)=0;
  virtual void SetStateContext(IStateContext * const pStateContext)=0;
  
protected:
  virtual void AddFunction(uint32_t source, uint32_t target, pt2Member memberFnc)=0;
};
};
#endif
/* ISTATETRANSITIONS_H_ */

/***************************************************************************
    begin                : 2016
    copyright            : (C) 2016 by mry
    email                : mry@hispeed.ch
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#ifndef IUCSISTATECONTEXT_H_
#define IUCSISTATECONTEXT_H_

#include "rfw/utils/ctypes.h"
#include "rfw/utils/cmessage.h"
#include "rfw/utils/ctimermessage.h"

namespace statemachine
{

class IStateMachineData;

/***********************************************************************//**
  @class  : IStateContext
  @package: Utils
***************************************************************************/
class IStateContext
{
  /// this class can not be copied
  UNCOPYABLE(IStateContext);

public:
  IStateContext();
  virtual ~IStateContext();

  virtual void Event(SMART_PTR(utils::CMessage) pMsg)=0;
  virtual void TimerEvent()=0;
  virtual void StartTimer(uint64_t nTimeInUSec)=0;
  virtual void StopTimer()=0;
  virtual void SetNextState(uint32_t invokingState, uint32_t nextState)=0;
  virtual void SetStateMachineData(IStateMachineData * pStateMachineData)=0;
  virtual IStateMachineData * const GetStateMachineData()=0;
};
};
#endif
/* ISTATECONTEXT_H_ */

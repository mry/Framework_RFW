# CMake for subpackage periphery
cmake_minimum_required (VERSION 2.8) 

INCLUDE_DIRECTORIES(
	${RFW_UTILS_INCLUDE_DIR}
	include
	)


file(GLOB_RECURSE ${PROJECT_NAME}_SRC 
    ${CMAKE_CURRENT_SOURCE_DIR}/src/*.cpp
    ${CMAKE_CURRENT_SOURCE_DIR}/src/*.c
    ${CMAKE_CURRENT_SOURCE_DIR}/include/*.h)

add_library(periphery STATIC ${${PROJECT_NAME}_SRC})

INSTALL(TARGETS periphery DESTINATION ${RFW_DEPLOY_DIR}/lib)

INSTALL(DIRECTORY ${RFW_PERIPHERY_INCLUDE_DIR}/rfw/periphery
	DESTINATION ${RFW_DEPLOY_DIR}/include/rfw
	PATTERN .svn EXCLUDE)

MESSAGE("Package periphery:Build type is: ${CMAKE_BUILD_TYPE}")

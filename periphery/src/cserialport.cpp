/***************************************************************************
    begin                : 2016
    copyright            : (C) 2016 by mry
    email                : mry@hispeed.ch
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#include "rfw/periphery/cserialport.h"
#include "rfw/utils/clogger.h"

#include <fcntl.h>
#include <stdio.h>
#include <string.h>
#include <termios.h>
#include <unistd.h>

#include <sys/ioctl.h>

using namespace utils;

namespace periphery
{
/***********************************************************************//**
  @method : CSerialport
  @comment: constructor
  @param:   port port to open
***************************************************************************/
CSerialport::CSerialport(std::string port)
: m_nLocalFd{0}
{
  ::strncpy(m_pPort, port.c_str(), sizeof(m_pPort));
}

/***********************************************************************//**
  @method : ~CSerialport
  @comment: destructor
***************************************************************************/
CSerialport::~CSerialport()
{
  if (m_nLocalFd){
    ::close (m_nLocalFd);
  }
}

/***********************************************************************//**
  @method : SetDtr
  @comment: set DTR
  @param:   bDtrSig
  @return:  DTR set
***************************************************************************/
bool CSerialport::SetDtr (bool bDtrSig)
{
  bool retVal = false;
  if ( m_nLocalFd ){
    int16_t status;
    struct termios tio;

    ::tcgetattr ( m_nLocalFd, &tio );
    tio.c_cflag &= ~HUPCL;
    ::tcsetattr ( m_nLocalFd, TCSANOW, &tio );
    ::ioctl ( m_nLocalFd, TIOCMGET, &status );

    if ( bDtrSig ){
      status &= ~TIOCM_DTR;
    }else{
      status |=  TIOCM_DTR;
    }
    ::ioctl ( m_nLocalFd, TIOCMSET, &status );
    retVal = true;
  }
  return retVal;
}

/***********************************************************************//**
  @method : SetRts
  @comment: set RTS signal
  @return:  true RTS signal set
***************************************************************************/
bool CSerialport::SetRts (bool bRtsSig)
{
  bool retVal = false;
  if (m_nLocalFd){
    int16_t status;
    struct termios tio;
    ::tcgetattr ( m_nLocalFd, &tio );
    tio.c_cflag &= ~HUPCL;
    ::tcsetattr ( m_nLocalFd, TCSANOW, &tio );
    ::ioctl ( m_nLocalFd, TIOCMGET, &status );

    if ( bRtsSig ){
      status &= ~TIOCM_RTS;
    }else{
      status |=  TIOCM_RTS;
    }
    ::ioctl ( m_nLocalFd, TIOCMSET, &status );
    retVal = true;
  }
  return retVal;
}

/***********************************************************************//**
  @method : SetBaud
  @comment: set baudrate 300 .. 11520000 default 19200
  @param:   lBaud baudrate
  @return:  baudrate set
***************************************************************************/
bool CSerialport::SetBaud (uint32_t lBaud)
{
  bool retVal = false;
  uint32_t baud;

  switch ( lBaud ){
  case 300   :   baud = B300;    break;
  case 600   :   baud = B600;    break;
  case 1200  :   baud = B1200;   break;
  case 2400  :   baud = B2400;   break;
  case 9600  :   baud = B9600;   break;
  case 19200 :   baud = B19200;  break;
  case 38400 :   baud = B38400;  break;
  case 57600 :   baud = B57600;  break;
  case 115200:   baud = B115200; break;
  case 230400:   baud = B230400; break;
  case 460800:   baud = B460800; break;
  case 500000:   baud = B500000; break;
  case 576000:   baud = B576000; break;
  case 921600:   baud = B921600; break;
  case 1000000:  baud = B1000000;break;
  case 11520000: baud = B1152000;break;

  default :
  {
    baud = B19200;
    WARN("Baudrate set to 19200 (is this what you want?)");
  }
    break;
  }

  if (m_nLocalFd){
    struct termios tio;
    tcgetattr(m_nLocalFd,&tio);
    tio.c_cflag  =   baud | CS8 | CREAD| CLOCAL;
    tio.c_cflag &= ~ HUPCL;
    tio.c_lflag  =   0;
    tio.c_iflag  =   IGNPAR;
    tio.c_oflag  =   0;
    tio.c_cc [VTIME] = 0;
    tio.c_cc [VMIN ] = 1;
    ::tcflush   ( m_nLocalFd, TCIFLUSH );
    ::tcsetattr ( m_nLocalFd, TCSANOW, &tio);
    ::fcntl     ( m_nLocalFd, F_SETFL, FNDELAY );
    DEBUG2("baudrate set.");
    retVal = true;
  }else{
    ERR("baudrate not set");
  }
  return retVal;
}

/***********************************************************************//**
  @method : OpenPort
  @comment: open port
  @return:  port opened
***************************************************************************/
bool CSerialport::OpenPort ()
{
  bool retVal = false;
  m_nLocalFd = ::open ( m_pPort, O_RDWR | O_NDELAY | O_NOCTTY );

  if (m_nLocalFd){
    DEBUG2("port opened" << m_pPort);
    retVal = true;
  }else{
    ERR("port not opened");
  }
  return retVal;
}

/***********************************************************************//**
  @method : ClosePort
  @comment: close port
  @return:  port closed
***************************************************************************/
bool CSerialport::ClosePort ()
{
  bool retVal = false;
  if(m_nLocalFd){
    ::close (m_nLocalFd);
    m_nLocalFd = 0;
    DEBUG2("port closed");
    retVal = true;
  }else{
    ERR("port not closed");
  }
  return retVal;
}

/***********************************************************************//**
  @method : SendStr
  @comment: send string
  @param:   data pointer to data
  @param:   size length of data
  @return:  >=0 data sent,
            < 0 error code
***************************************************************************/
int32_t CSerialport::SendStr (uint8_t const * const data, 
                              uint16_t size)
{
  int32_t retVal = 0;
  if (m_nLocalFd){
    retVal = ::write (m_nLocalFd,data,size);
    DEBUG2((char*)data);
  }
  return retVal;
}

/***********************************************************************//**
  @method : ReadStr
  @comment: read string
  @param:   data pointer to data
  @param:   len length of data
  @param:   blocking ReadStr as blocking operation
  @return:  >=0 data read,
      < 0 error code
***************************************************************************/
int32_t CSerialport::ReadStr (uint8_t * data, 
                              uint16_t len, 
                              bool blocking)
{
  if (blocking){
    /* maximum bit entry (fd) to test */
    int maxfd = m_nLocalFd+1;

    /* file descriptor set */
    fd_set readfs;

    /* set testing for source 1 */
    FD_SET(m_nLocalFd, &readfs);

    /* block until input becomes available */
    select(maxfd, &readfs, NULL, NULL, NULL);
  }

  int32_t retVal = 0;
  if (m_nLocalFd){
    retVal = ::read (m_nLocalFd, data, len);
  }
  return retVal;
}
}
/* END_MODULE */

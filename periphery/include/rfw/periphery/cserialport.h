/***************************************************************************
    begin                : 2016
    copyright            : (C) 2016 by mry
    email                : mry@hispeed.ch
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#ifndef CSERIALPORT_H
#define CSERIALPORT_H

#include "rfw/periphery/iserialport.h"

#include <string>

namespace periphery
{

/***********************************************************************//**
  @class  : CSerialport
  @package: Utils
***************************************************************************/
class CSerialport 
: public ISerialport
{
  /// this class can not be copied
  UNCOPYABLE(CSerialport);

public:
  CSerialport(std::string port);
  virtual ~CSerialport();

public:
  bool SetDtr(bool bDtrSig) override;
  bool SetRts(bool bRtsSig) override;
  bool SetBaud(uint32_t lBaud) override;
  bool OpenPort() override;
  bool ClosePort() override;
  int32_t SendStr(uint8_t const * const  data, uint16_t size) override;
  int32_t ReadStr(uint8_t * data, uint16_t len, bool blocking) override;

private:
  uint16_t m_nLocalFd;
  char m_pPort [32];
};
};
#endif
/* CSERIALPORT_H */

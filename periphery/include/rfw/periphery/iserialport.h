/***************************************************************************
    begin                : 2016
    copyright            : (C) 2016 by mry
    email                : mry@hispeed.ch
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#ifndef ISERIALPORT_H
#define ISERIALPORT_H

#include "rfw/utils/ctypes.h"
#include <cstdint>

namespace periphery
{

/***********************************************************************//**
  @class  : Iserialport
  @package: Utils
***************************************************************************/
class ISerialport
{
  /// this class can not be copied
  UNCOPYABLE(ISerialport);
public:
  ISerialport();
  virtual ~ISerialport();
  virtual bool SetDtr    (bool bDtrSig     ) = 0;
  virtual bool SetRts    (bool bRtsSig     ) = 0;
  virtual bool SetBaud   (uint32_t lBaud   ) = 0;
  virtual bool OpenPort  () = 0;
  virtual bool ClosePort () = 0;
  virtual int32_t SendStr(uint8_t const * const data, uint16_t size) = 0;
  virtual int32_t ReadStr(uint8_t * data, uint16_t len, bool blocking) = 0;
};
};
#endif
/* ISERIALPORT_H */

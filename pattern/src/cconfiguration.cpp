/***************************************************************************
    begin                : 2016
    copyright            : (C) 2016 by mry
    email                : mry@hispeed.ch
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#include "rfw/pattern/cconfiguration.h"
#include "rfw/pattern/iconfigitem.h"

#include <iostream>
#include <string>
#include <set>
#include <sstream>
#include <exception>
#include <fstream>

#include <boost/config.hpp>
#include <boost/program_options/detail/config_file.hpp>
#include <boost/program_options/parsers.hpp>
#include <boost/program_options/config.hpp>
#include <boost/program_options/options_description.hpp>
#include <boost/program_options/value_semantic.hpp>
#include <boost/program_options/variables_map.hpp>

namespace pattern
{

/***********************************************************************//**
  @method :  CConfiguration
  @comment:  constructor
***************************************************************************/
CConfiguration::CConfiguration() 
: m_bValid{false}
{
}

/***********************************************************************//**
  @method :  DoProcess
  @comment:  process the input file
  @param  :  filename name to process
  @return :  true successful, false failed
***************************************************************************/
bool CConfiguration::DoProcess(const char* filename)
{
  m_bValid = Process(filename);
  return m_bValid;
}

/***********************************************************************//**
  @method :  RegisterConfigItem
  @comment:  register a config item
  @param  :  pItem item to register
***************************************************************************/
void CConfiguration::RegisterConfigItem(IConfigItem* pItem)
{
  std::unique_lock<std::mutex> lock(m_sMutex);
  m_lstConfigItems.push_back(pItem);
}

/***********************************************************************//**
  @method :  Valid
  @comment:  true if configuration valid
***************************************************************************/
bool CConfiguration::Valid() const
{
  return m_bValid;
}

/***********************************************************************//**
  @method :  AssignParseOptions
  @comment:  assign parse options from items
  @param  :  desc_file descriptor for boost
***************************************************************************/
void CConfiguration::AssignParseOptions(boost::program_options::options_description & desc_file)
{
  /// Assign options to parse
  std::unique_lock<std::mutex> lock(m_sMutex);
  std::list<IConfigItem*>::iterator it = m_lstConfigItems.begin();
  while(it != m_lstConfigItems.end()){
    (*it)->AddOptions(desc_file);
    it++;
  }
}

/***********************************************************************//**
  @method :  GetParsedOptions
  @comment:  get parsed options and assign options to items
  @param  :  vm_cmd variables map for boost
***************************************************************************/
void CConfiguration::GetParsedOptions(boost::program_options::variables_map& vm_cmd)
{
  /// Get options and assign data to registered items
  std::unique_lock<std::mutex> lock(m_sMutex);
  std::list<IConfigItem*>::iterator it = m_lstConfigItems.begin();
  while(it != m_lstConfigItems.end()){
    (*it)->GetOptions(vm_cmd);
    it++;
  }
}

/***********************************************************************//**
  @method :  Process
  @comment:  process configuration file
  @param  :  filename name of configuration file
  return  :  true successfully parsed
***************************************************************************/
bool CConfiguration::Process(const char* filename)
{
  m_bValid = false;

  namespace pod = boost::program_options;
  pod::variables_map vm_cmd;
  pod::options_description desc_file("config file");

  /// Assign options to parse
  AssignParseOptions(desc_file);

  std::ifstream config(filename);
  if(!config){
    return false;
  }

  if(config.is_open()){

    // Parse Config file
    pod::store(pod::parse_config_file(config, desc_file), vm_cmd);
    pod::notify(vm_cmd);
    GetParsedOptions(vm_cmd);
  }else{
    return false;
  }

  m_bValid = true;
  return true;
}
}
/* END_MODULE */

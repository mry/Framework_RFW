/***************************************************************************
    begin                : 2016
    copyright            : (C) 2016 by mry
    email                : mry@hispeed.ch
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#include "rfw/pattern/csubject.h"
#include "rfw/pattern/iobserver.h"
#include "rfw/utils/ctypes.h"

namespace pattern
{
/***********************************************************************//**
  @method :  CSubject
  @comment:  constructor
***************************************************************************/
CSubject::CSubject()
{
}

/***********************************************************************//**
  @method :  ~CSubject
  @comment:  destructor
***************************************************************************/
CSubject::~CSubject()
{
  m_listObservers.clear();
}

/***********************************************************************//**
  @method :  AttachObserver
  @comment:  attach an observer.
  @param  :  p_pObserver pointer to observer
  @return :  -
***************************************************************************/
void CSubject::AttachObserver (IObserver* p_pObserver)
{
  std::unique_lock<std::mutex> lock(m_sMutex);
  m_listObservers.push_back(p_pObserver);
}

/***********************************************************************//**
  @method :  DetachObserver
  @comment:  remove an observer
  @param  :  p_pObserver pointer to observer
  @return :  -
***************************************************************************/
void CSubject::DetachObserver (IObserver* p_pObserver)
{
  std::unique_lock<std::mutex> lock(m_sMutex);
  m_listObservers.remove(p_pObserver);
}

/***********************************************************************//**
  @method :  NotifyObservers
  @comment:  update all registered observers
  @param  :  -
  @return :  -
***************************************************************************/
void CSubject::NotifyObservers ()
{
  std::unique_lock<std::mutex> lock(m_sMutex);
  for (auto it: m_listObservers) {
    it->update();
  }
}
}
/* END_MODULE */

/***************************************************************************
    begin                : 2016
    copyright            : (C) 2016 by mry
    email                : mry@hispeed.ch
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#ifndef CSUBJECT_H
#define CSUBJECT_H

#include <list>
#include <mutex>

#include "rfw/utils/ctypes.h"

namespace pattern
{

class IObserver;

/***********************************************************************//**
  @class  : CSubject
  @package: Utils
***************************************************************************/
class CSubject
{
  /// this class can not be copied
  UNCOPYABLE(CSubject);

public:
  CSubject();
  virtual ~CSubject();
  virtual void AttachObserver (IObserver* p_pObserver);
  virtual void DetachObserver (IObserver* p_pObserver);
  virtual void NotifyObservers ();

private:
  /// observers
  std::list<IObserver*> m_listObservers;
  
  /// mutex internal use
  std::mutex m_sMutex;
};
};
#endif
/* CSUBJECT_H */

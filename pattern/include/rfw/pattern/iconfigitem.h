/***************************************************************************
    begin                : 2016
    copyright            : (C) 2016 by mry
    email                : mry@hispeed.ch
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#ifndef ICONFIGITEM_H
#define ICONFIGITEM_H

#include <boost/program_options/config.hpp>
#include <boost/program_options/parsers.hpp>
#include <boost/program_options/options_description.hpp>
#include <boost/program_options/value_semantic.hpp>
#include <boost/program_options/variables_map.hpp>

namespace pattern
{
/***********************************************************************//**
  @class  : IUCSIConfigItem
  @package: Utils
***************************************************************************/
class IConfigItem
{
  public:
    IConfigItem() = default;
    virtual ~IConfigItem() = default;

  /***********************************************************************//**
  @method :  AddOptions
  @comment:  Add the desired options
  @param  :  desc options descriptor
  ***************************************************************************/
  virtual void AddOptions( boost::program_options::options_description & desc ) = 0;

  /***********************************************************************//**
  @method :  GetOptions
  @comment:  Get the desired options
  @param  :  varMap variables map to be configured
  ***************************************************************************/
  virtual void GetOptions (boost::program_options::variables_map& varMap ) = 0;
};
};
#endif
/* IUCSICONFIGITEM_H */

/***************************************************************************
    begin                : 2016
    copyright            : (C) 2016 by mry
    email                : mry@hispeed.ch
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#ifndef COBSERVER_H
#define COBSERVER_H

namespace pattern
{

/***********************************************************************//**
  @class  : CObserver
  @package: Utils
***************************************************************************/
class CObserver
{
public:
  CObserver() = default;
  virtual ~CObserver()= default;
};
};
#endif
/* COBSERVER_H */


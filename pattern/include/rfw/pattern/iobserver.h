/***************************************************************************
    begin                : 2016
    copyright            : (C) 2016 by mry
    email                : mry@hispeed.ch
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#ifndef IOBSERVER_H
#define IOBSERVER_H

/***********************************************************************//**
  @class  : IObserver
  @package: Utils
***************************************************************************/
namespace pattern
{
class IObserver
{
public:
  virtual ~IObserver() = default;
  // implement this
  virtual void update () = 0;
  
protected:
  IObserver() = default;
};
};
#endif
/*IOBSERVER_H*/

/***************************************************************************
    begin                : 2016
    copyright            : (C) 2016 by mry
    email                : mry@hispeed.ch
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#ifndef CCONFIGURATION_H
#define CCONFIGURATION_H

#include "rfw/utils/ctypes.h"

#include <string>
#include <list>
#include <mutex>

#include <boost/program_options/options_description.hpp>
#include <boost/program_options/variables_map.hpp>

namespace pattern
{

/// forward declaration
class IConfigItem;

/***********************************************************************//**
  @class  : CConfiguration
  @package: Utils
***************************************************************************/
class CConfiguration
{
  /// this class can not be copied
  UNCOPYABLE(CConfiguration);

public:
  CConfiguration();
  ~CConfiguration() = default;
  bool DoProcess(const char* filename);
  bool Valid() const;
  void RegisterConfigItem(IConfigItem* pItem);

private:
  void AssignParseOptions(boost::program_options::options_description & desc_file);
  void GetParsedOptions(boost::program_options::variables_map& vm_cmd);
  bool Process(const char* filename);

  /// parsed configuration valid
  bool m_bValid;

  /// mutex internal use
  std::mutex m_sMutex;

  /// config list internal use
  std::list<IConfigItem*> m_lstConfigItems;
};
};
#endif
/* CCONFIGURATION_H */

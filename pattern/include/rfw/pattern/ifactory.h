/***************************************************************************
    begin                : 2016
    copyright            : (C) 2016 by mry
    email                : mry@hispeed.ch
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#ifndef IFACTORY_H
#define IFACTORY_H

namespace pattern
{
/***********************************************************************//**
  @class  : IFactory
  @package: Utils
***************************************************************************/
class IFactory
{
  public:
    IFactory() = default;
    virtual ~IFactory() = default;
    
    virtual void Setup() = 0;
    virtual void Start() = 0;
    virtual void Stop() = 0;
};
};
#endif
/* IFACTORY_H */

/***************************************************************************
    begin                : 2016
    copyright            : (C) 2016 by mry
    email                : mry@hispeed.ch
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#ifndef CSEMAPHORE_H
#define CSEMAPHORE_H

#include "rfw/utils/ctypes.h"
#include <condition_variable>
#include <mutex>

namespace utils
{


/***********************************************************************//**
  @class  : CSemaphore
  @package: Utils
***************************************************************************/
class CSemaphore
{
/// this class can not be copied
UNCOPYABLE(CSemaphore);

public:
CSemaphore();
~CSemaphore();

/***********************************************************************//**
  @method :  SignalEvent
  @comment:  signal an event
***************************************************************************/
inline void SignalEvent()
{
  std::unique_lock<std::mutex> lock(m_mtx);
  ++m_count;
  m_cv.notify_one();
};

/***********************************************************************//**
  @method :  WaitEvent
  @comment:  wait for an event
  @return :  0 ok
***************************************************************************/
inline int32_t WaitEvent()
{
  std::unique_lock<std::mutex> lock(m_mtx);
  while(m_count == 0)
  {
      m_cv.wait(lock);
      return 0;
  }
  -- m_count;
  return 0;
};

private:
  std::condition_variable m_cv;
  std::mutex m_mtx;
  size_t m_count;
};

};
#endif
/* CSEMAPHORE_H */

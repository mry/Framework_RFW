/***************************************************************************
    begin                : 2016
    copyright            : (C) 2016 by mry
    email                : mry@hispeed.ch
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#ifndef CEVENTMESSAGE_H
#define CEVENTMESSAGE_H

#include "rfw/utils/cmessage.h"
#include "rfw/utils/ctypes.h"

#include <string>

namespace utils
{

/***********************************************************************//**
  @class  : CEventMessage
  @package: Utils
***************************************************************************/
class CEventMessage 
: public CMessage
{
  /// this class can not be copied
  UNCOPYABLE(CEventMessage);

public:
  CEventMessage();
  explicit CEventMessage(int32_t);
  virtual ~CEventMessage();
  virtual int32_t GetMessageId() const;
  virtual std::string ToString() const;
  void SetCommand(int32_t command);
  int32_t GetCommand() const;

private:
  int32_t m_nCommandID;
};

};
#endif
/* CEventMessage_H */

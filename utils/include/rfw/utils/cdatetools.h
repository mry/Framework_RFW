/***************************************************************************
    begin                : 2017
    copyright            : (C) 2017 by mry
    email                : mry@hispeed.ch
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#ifndef _DATE_TOOLS_H
#define _DATE_TOOLS_H

#include <vector>
#include <ostream>
#include <sys/time.h>

namespace utils {

typedef enum {
    Sunday = 0, Monday, Tuesday, Wednesday, Thursday, Friday, Saturday
} Weekday;

typedef enum {
    January = 0, February, March, April, May, June, July, August, September, October, November, December
} Month;

/** Class that supports various date and time calculations */
class CDateTime
{
private:
    struct timeval m_timeval;
public:
    /** Current date-time */
    CDateTime();
    /** Construct from sec.usec since epoch (UTC) */
    CDateTime(time_t secs, suseconds_t usecs = 0);
    CDateTime(struct timeval tv) : m_timeval(tv) {}
    /** Copy constuctor */
    CDateTime(const CDateTime& _copy);

    /** Adds \a _hours hours to the time and normalizes the CDateTime */
    CDateTime addHour(const int _hours) const;
    /** Adds \a _minutes minutes to the time and normalizes the CDateTime */
    CDateTime addMinute(const int _minutes) const;
    /** Adds \a _seconds seconds to the time and normalizes the CDateTime */
    CDateTime addSeconds(const int _seconds) const;
    /** Adds \a _month months to the date and normalizes the CDateTime */
    CDateTime addMonth(const int _month) const;
    /** Adds \a _years years to the date and normalizes the CDateTime */
    CDateTime addYear(const int _years) const;
    /** Adds \a _days days to the date and normalizes the CDateTime */
    CDateTime addDay(const int _days) const;

    /** Returns the day of month */
    int getDay() const;
    /** Returns the month */
    int getMonth() const;
    /** Returns the year */
    int getYear() const;

    /** Returns the hour */
    int getHour() const;
    /** Returns the minute */
    int getMinute() const;
    /** Returns the second */
    int getSecond() const;

    /** Returns the day of year */
    int getDayOfYear() const;
    /** Returns the weekday */
    Weekday getWeekday() const;

    /** Returns true if the instance and \a _other represent the same date and time */
    bool operator==(const CDateTime& _other) const;
    /** Returns true if the instance and \a _other do not represent the same time and date */
    bool operator!=(const CDateTime& _other) const;
    /** Returns true if the instance is before _other.
      * @see before */
    bool operator<(const CDateTime& _other) const;
    /** Returns true if the instance is after _other.
      * @see after */
    bool operator>(const CDateTime& _other) const;
    /** Returns true if the instance is before or equal _other.
      * @see before */
    bool operator<=(const CDateTime& _other) const;
    /** Returns true if the instance is after or equal _other.
      * @see after */
    bool operator>=(const CDateTime& _other) const;

    /** Returns the difference in seconds */
    int difference(const CDateTime& _other) const;

    /** Returns the seconds since epoch */
    time_t secondsSinceEpoch() const;

    /** Returns the offset in seconds from GMT */
    long int getTimezoneOffset() const;

    operator std::string() const;
    std::string toString() const;

    std::string toRFC2822String() const;

    /**
     * Parses RFC2445 date-time
     * @param _isoStr CDateTime string formatted as "yyyymmddThhmmss[Z]"
     * @throw invalid_argument if a malformatted \a _isoStr is provided
     */
    static CDateTime parseRFC2445(const std::string& _isoStr);

    /**
     * Emit RFC2445 date-time format
     */
    std::string toRFC2445IcalDataTime() const;

    /**
     * parseISO8601 -- ISO8601 or similar RFC3339
     * http://www.cl.cam.ac.uk/~mgk25/iso-time.html
     * http://www.cs.tut.fi/~jkorpela/iso8601.html
     * http://www.ietf.org/rfc/rfc3339.txt
     */
    static CDateTime parseISO8601(std::string in);

    /**
     * Emit ISO8601 or RFC3339 format
     * http://www.cl.cam.ac.uk/~mgk25/iso-time.html
     * http://www.cs.tut.fi/~jkorpela/iso8601.html
     * http://www.ietf.org/rfc/rfc3339.txt
     */
    std::string toISO8601() const;

    /**
     * Will emit ISO8601 format with timezone appended
     *
     * Various versions are floating around for the timezone
     * format: e.g. 02:00, 0200, 02 and not all entities
     * accept all formats
     * We emit it as '+hh:mm' since that is the format
     * mentioned in the public rfc3339 specification[2]
     * It's recomended to emit the time in UTC where no
     * timezone needs to be appended
     *
     * @see toISO8601
     * [1] http://www.cs.tut.fi/~jkorpela/iso8601.html
     * [2] http://www.ietf.org/rfc/rfc3339.txt
     */
    std::string toISO8601_local() const;

    /**
     * Emit ISO8601 or RFC3339 with ms precision
     * http://www.cl.cam.ac.uk/~mgk25/iso-time.html
     * http://www.cs.tut.fi/~jkorpela/iso8601.html
     * http://www.ietf.org/rfc/rfc3339.txt
     * TODO probably non-standard:
     * - rfc3339 has 100 ms digit precision
     * - ISO8601 seems seconds only precision(spec is 140$)
     *
     * Various versions are floating around for the timezone
     * format: e.g. 02:00, 0200, 02 and not all entities
     * accept all formats
     * We emit it as '+hh:mm' since that is the format
     * mentioned in the public rfc3339 specification[2]
     * It's recomended to emit the time in UTC where no
     * timezone needs to be appended
     */
    std::string toISO8601_ms_local() const;

    /**
     * Emit ISO8601 or RFC3339 with ms precision
     * http://www.cl.cam.ac.uk/~mgk25/iso-time.html
     * http://www.cs.tut.fi/~jkorpela/iso8601.html
     * http://www.ietf.org/rfc/rfc3339.txt
     * TODO probably non-standard:
     * - rfc3339 has 100 ms digit precision
     * - ISO8601 seems seconds only precision(spec is 140$)
     */
    std::string toISO8601_ms() const;

    /**
     * Parses human readable "2014-08-07 23:33:30" format
     * @throw invalid_argument if parsing fails
     */
    static CDateTime parsePrettyString(const std::string& strTime);

    /**
     * Emits human readable "2014-08-07 23:33:30" format
     */
    std::string toPrettyString() const;

    /** The NullDate has it's date and time parts set to 0. It should
      * be used for default values. */
    static CDateTime NullDate;
};
std::ostream& operator<<(std::ostream& out, const CDateTime& _dt);

}
#endif

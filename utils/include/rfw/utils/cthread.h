/***************************************************************************
    begin                : 2016
    copyright            : (C) 2016 by mry
    email                : mry@hispeed.ch
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#ifndef CTHREAD_H
#define CTHREAD_H

#include "rfw/utils/csemaphore.h"
#include "rfw/utils/ctypes.h"

#include <list>
#include <mutex>
#include <thread>

namespace utils
{

/***********************************************************************//**
 * Forward Declaration and Defines
 ***************************************************************************/
class CMessage;

typedef SMART_PTR(CMessage) smartMessagePtr;

/***********************************************************************//**
 @class  : CThread
 @package: Utils
 ***************************************************************************/
class CThread 
{
  /// this class can not be copied
  UNCOPYABLE(CThread);

public:
  explicit CThread(uint32_t threadId);
  virtual ~CThread();

public:
  virtual bool StartThread();
  virtual bool StopThread();

  void TransmitMessage(smartMessagePtr pMsg);

  uint32_t GetThreadId();

  bool SetThreadPriority(int32_t prio);
  bool GetThreadPriority(int32_t &priority);

  uint32_t GetPendingMessages();
  inline bool IsRunning() 
  {
    return m_bRunning;
  }
  
  bool SendTo(uint32_t threadId, smartMessagePtr pMessage);

protected:
  smartMessagePtr ReceiveMessage();

protected:
  //pure virtual
  virtual void * Run(void * args)=0;

private:
  void SetEvent();
  int32_t WaitEvent();

  //Attributes
private:
  bool m_bRunning;
  uint32_t m_nThreadId;
  std::thread m_workerThread;

  // mutex internal use
  std::mutex m_sMutex;

  // Semaphore internal use
  CSemaphore m_sSemaphore;

  // message list internal use
  std::list<smartMessagePtr> m_listMessageQueue;
};

};
#endif
/* CTHREAD_H */

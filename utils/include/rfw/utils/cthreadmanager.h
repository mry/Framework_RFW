/***************************************************************************
    begin                : 2016
    copyright            : (C) 2016 by mry
    email                : mry@hispeed.ch
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#ifndef CTHREADMANAGER_H
#define CTHREADMANAGER_H

#include "rfw/utils/ctypes.h"
#include "rfw/utils/csemaphore.h"

#include <map>
#include <optional>


namespace utils
{

class CThread;
class CMessage;

typedef std::map<uint32_t, CThread* const> CThreadMap_t;
typedef SMART_PTR(CMessage) smartMessagePtr;

/***********************************************************************//**
  @class  : CThreadManager
  @package: Utils
***************************************************************************/
class CThreadManager
{
  /// this class can not be copied
  UNCOPYABLE(CThreadManager);

public:
  ~CThreadManager();
  static CThreadManager& GetInstance();

private:
  CThreadManager();

public:
  void RegisterThread(uint32_t threadId, CThread * const pThread);
  void DeregisterThread(uint32_t threadId);
  CThread * const GetThread (uint32_t threadId);
  bool Send(uint32_t senderId, uint32_t receiverId, smartMessagePtr pMessage);
  bool SendSync(uint32_t senderId, uint32_t receiverId, CMessage* pMessage);

private:
  std::map<uint32_t,  CThread *const> m_mapDictionary;
};
};
#endif
/* CTHREADMANAGER_H */

/***************************************************************************
    begin                : 2016
    copyright            : (C) 2016 by mry
    email                : mry@hispeed.ch
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#ifndef CEVENTUTILS_H_
#define CEVENTUTILS_H_

#include "rfw/utils/ceventmessage.h"
#include "rfw/utils/cthreadmanager.h"
#include "rfw/utils/ctypes.h"

namespace utils
{
#define SEND_EVENT( threadId, command) \
  boost::shared_ptr<CEventMessage> pMessage(new CEventMessage(command)); \
  SMART_PTR(CMessage) smartMessagePtr = CAST_SMART_PTR(CMessage,pMessage); \
  CThreadManager::GetInstance().Send(threadId, smartMessagePtr);
};

#endif

/* CEVENTUTILS_H_ */

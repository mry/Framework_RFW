/***************************************************************************
    begin                : 2016
    copyright            : (C) 2016 by mry
    email                : mry@hispeed.ch
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#ifndef CMESSAGE_H
#define CMESSAGE_H

#include "rfw/utils/ctypes.h"

#include <mutex>
#include <string>

namespace utils
{

typedef enum
{
  MESSAGE_EVENT_e      = 1,
  EVENT_TIMER_e        = 2,
  EVENT_LAST_e         = 3
}MESSAGE_TYPE_e;

//**************************************************************************
/// Forward declaration
class CSemaphore;

/***********************************************************************//**
  @class  : CMessage
  @package: Utils
***************************************************************************/
class CMessage
{
  /// this class can not be copied
  UNCOPYABLE(CMessage);

public:
  CMessage();
  virtual ~CMessage();
  static uint32_t GetNumberOfExistingMessages();

  // implement this
  virtual int32_t GetMessageId() const = 0;
  virtual std::string ToString() const = 0;
  void SetSenderId(uint32_t threadId) {m_nSenderThreadId = threadId;}
  uint32_t GetSenderId() {return m_nSenderThreadId;}
  void SetSemaphore( CSemaphore* pSema);

private:
  uint32_t m_nSenderThreadId; /// id of sender
  CSemaphore* m_pSema;    /// synchronization
  
  //TODO protect with mutex.
  static uint32_t m_nExistingMessages; /// global message count
  static std::mutex m_MessageCtrMutex;
};
};
#endif
/* CMESSAGE_H */

/***************************************************************************
    begin                : 2016
    copyright            : (C) 2016 by mry
    email                : mry@hispeed.ch
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#ifndef CMESSAGEBUSREACTOR_H_
#define CMESSAGEBUSREACTOR_H_

#include "rfw/utils/clogger.h"
#include "rfw/utils/cmessage.h"
#include "rfw/utils/cthread.h"
#include "rfw/utils/ctypes.h"

#include <functional>
#include <map>

namespace utils
{

/***********************************************************************//**
  @comment: How to use this class

  Derive from class CMessageBusReactor
  The last entry in the header file defines the reactors for the messages:
    DEFINE_REACTOR_HANDLER( MessageClassName, MyClassName);

  In the body (constructor) create the reactor and register it:
    CONSTRUCT_REACTOR_HANDLER(MessageClassName)
    REGISTER_REACTOR_HANDLER(MessageClassName,MyClassName);

   Here comes an example:

----------------------------------------------------------------------------
HEADER:

  class Client: public CMessageBusReactor
  {
  public:
    Client(int32_t threadId);
    virtual ~Client();
    void HandleCUCSITestMessage(SMART_PTR<CUCSITestMessage> Msg){DEBUG2("Handle CUCSITestMessage");};
    DEFINE_REACTOR_HANDLER(CUCSITestMessage,Client);
  };

----------------------------------------------------------------------------
BODY:

  Client::Client(int32_t threadId):
  CMessageBusReactor(threadId),
  CONSTRUCT_REACTOR_HANDLER(CUCSITestMessage)
  {
    REGISTER_REACTOR_HANDLER(CUCSITestMessage,Client);
  }

----------------------------------------------------------------------------
How to send message from other thread:

  SMART_PTR<CTestMessage>msg(new CTestMessage);
  SendTo(threadId,msg);

 ***************************************************************************/

/***********************************************************************//**
 @comment: Add this to your header file
 ***************************************************************************/
#define DEFINE_REACTOR_HANDLER(_MESSAGE,_CLASSNAME) \
  CMessageBusReactorHandler<_MESSAGE, _CLASSNAME, &_CLASSNAME::Handle##_MESSAGE> m_Handler##_MESSAGE;

/***********************************************************************//**
 @comment: Add this to the constructor of your class
 of type CMessageBusReactor
 ***************************************************************************/
#define CONSTRUCT_REACTOR_HANDLER(_MESSAGE) \
  m_Handler##_MESSAGE(this)

/***********************************************************************//**
 @comment: Add this to the initializing part of your class
 of type CMessageBusReactor
 ***************************************************************************/
#define REGISTER_REACTOR_HANDLER(_MESSAGE, _CLASSNAME) \
{ \
  _MESSAGE Msg; \
  m_Handler##_MESSAGE.SetId(Msg.GetMessageId()); \
  this->AddHandlerFunction(Msg.GetMessageId(),  \
  std::bind( \
  &CMessageBusReactorHandler<_MESSAGE, _CLASSNAME, &_CLASSNAME::Handle##_MESSAGE>::Handle,&m_Handler##_MESSAGE,_1)); \
};

/***********************************************************************//**
 @class  : CMessageBusReactorHandlerBase
 @comment:
 ***************************************************************************/
class IMessageBusReactorHandlerBase 
{
  public:
    IMessageBusReactorHandlerBase() :
      m_MessageId(-1)
    {
    }

    virtual ~IMessageBusReactorHandlerBase()
    {
    }

    void SetId(int32_t msgId) {
      m_MessageId = msgId;
    }

    int32_t GetId() {
      return m_MessageId;
    }

    virtual void Handle(std::shared_ptr<CMessage> pMsg)=0;

  private:
    int32_t m_MessageId;
};

/***********************************************************************//**
 @class  : CMessageBusReactorHandler
 @comment:
 ***************************************************************************/
template <typename _CMessageType,
          typename _HANDLER_CLASS,
          void (_HANDLER_CLASS::*_HANDLER_FUNCT)(std::shared_ptr<_CMessageType>)>
class CMessageBusReactorHandler
: public IMessageBusReactorHandlerBase 
{
public:
  typedef _CMessageType Message;
  typedef _HANDLER_CLASS HandlerObjectClass;

public:
  CMessageBusReactorHandler(HandlerObjectClass* pHandlerObject) :
    mpHandlerObject(pHandlerObject) {
  }

  virtual ~CMessageBusReactorHandler() {
  }

  void Handle(std::shared_ptr<CMessage> pMsg) {
    DEBUG2("CMessageType::handle");
    auto d {std::static_pointer_cast<_CMessageType>(pMsg)};
    (mpHandlerObject->*_HANDLER_FUNCT)(d);
  }

  void SetHandlerObject(HandlerObjectClass& handlerObject) {
    mpHandlerObject = &handlerObject;
  }

protected:
  HandlerObjectClass* mpHandlerObject; ///< pointer to object that handles the message
};

/// definition of handler function
typedef std::function<void(std::shared_ptr<CMessage>)> handlerFunction;

/// definition for function map
typedef std::map<int32_t, handlerFunction> MessageHandlerMap;

/// definition for function map iterator
typedef std::map<int32_t, handlerFunction>::iterator MessageHandlerMapIterator;

/***********************************************************************//**
 @class  : CMessageBusReactor
 @comment:
 ***************************************************************************/
class CMessageBusReactor
: public CThread 
{
  /// this class can not be copied
  UNCOPYABLE(CMessageBusReactor)

public:
  CMessageBusReactor(uint32_t threadId);
  virtual ~CMessageBusReactor();
  void AddHandlerFunction(int32_t messageId, handlerFunction memberFnc);

protected:
  virtual void* Run(void* args);

private:
  void ReceiveAndHandleMsg();
  MessageHandlerMap m_Functions;
};

};
#endif /* CMessageBusReactor_H_ */

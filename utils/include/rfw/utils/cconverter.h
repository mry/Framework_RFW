/***************************************************************************
                          cconverter.h  -  description
                             -------------------
    begin                : Tue Oct 11 2016
    copyright            : (C) 2016 by mry
    email                : mry@hispeed.ch

 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#ifndef CONVERTER_H
#define CONVERTER_H

#include <string>
#include <vector>

#include "rfw/utils/ctypes.h"

namespace utils
{

namespace converter
{
  std::string trim(const std::string& _str);

  int32_t strToInt(const std::string& _strValue);

  uint32_t  strToUInt(const std::string& _strValue);
  int32_t strToIntDef(const std::string& _strValue, const int32_t _default);
  uint32_t strToUIntDef(const std::string& _strValue, const uint32_t _default);
  uint64_t strToULongLong(const std::string& _strValue);
  uint64_t strToULongLongDef(const std::string& _strValue, const uint32_t _default);

  std::string intToString(const uint64_t, const bool _hex = false);
  std::string uintToString(const uint64_t, bool _hex = false);

  double strToDouble(const std::string& _strValue);
  double strToDoubleDef(const std::string& _strValue, const double _default);

  std::string unsignedLongIntToHexString(const uint64_t _value);
  std::string unsignedLongIntToString(const uint64_t _value);

  std::string doubleToString(const double _value);

  std::vector<std::string> splitString(const std::string& _source,
                                       const char _delimiter,
                                       bool _trimEntries);
};

};
#endif // CONVERTER_H

/***************************************************************************
    begin                : 2016
    copyright            : (C) 2016 by mry
    email                : mry@hispeed.ch
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#ifndef CTIMERMESSAGE_H
#define CTIMERMESSAGE_H

#include "rfw/utils/ctypes.h"
#include "rfw/utils/ceventmessage.h"

namespace utils
{

/***********************************************************************//**
  @class  : CTimerMessage
  @package: Utils
***************************************************************************/
class CTimerMessage 
: public CEventMessage
{
  /// this class can not be copied
  UNCOPYABLE(CTimerMessage);
  
public:
  CTimerMessage();
  virtual ~CTimerMessage();

  int32_t GetMessageId() const override;
  std::string ToString() const override;
};
};
#endif
/* CTIMERMESSAGE_H */

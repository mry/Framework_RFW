/***************************************************************************
    begin                : 2016
    copyright            : (C) 2016 by mry
    email                : mry@hispeed.ch
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#ifndef CTYPES_H
#define CTYPES_H

#include <stddef.h>

#include <cstdint>
#include <memory>

namespace utils
{
/***********************************************************************//**
 * @comment: definition for smart pointer
 *           usage: SMART_PTR(myClass) pMyClass ...
***************************************************************************/
#define SMART_PTR( classtype ) std::shared_ptr<classtype>

/***********************************************************************//**
 * @comment: template to create smart pointers
 *           usage: SMART_PTR(myClass) pMyClass = create<myClass>();
***************************************************************************/
template<typename classtype>
std::shared_ptr<classtype> create () {
  auto pMsg{std::make_shared<classtype>()};
  return pMsg;
};

/***********************************************************************//**
 * @comment: convenience to create smart pointers
 *           usage: SMART_PTR(myClass) pMyClass = CREATE_SMART_PTR(myClass);
***************************************************************************/
#define CREATE_SMART_PTR( classtype ) create<classtype>();

/***********************************************************************//**
 * @comment: convenience to cast smart pointers
 *           usage:
 *           SMART_PTR(myClass) pMyClass = CREATE_SMART_PTR(myClass);
 *           SMART_PTR(myDerivedClass) pMyDerivedClass = CAST_SMART_PTR(myDerivedClass,pMyClass);
***************************************************************************/
#define CAST_SMART_PTR( classtype, in ) std::static_pointer_cast<classtype>(in);

/***********************************************************************//**
  @comment:  uncopyable macro
             define copy and assignment operator as private.
             An implementation is intentionally omitted.
***************************************************************************/
#define UNCOPYABLE( classtype ) \
  private: \
  classtype(const classtype& right) = delete; \
  classtype & operator=(const classtype& right) = delete;

/***********************************************************************//**
  @method :  SafeDelete
  @comment:  safe delete operation to delete simple type, ptr is set to 0
  @param  :  ptr pointer to delete
***************************************************************************/
template <typename Type>
void SafeDelete(Type *&ptr) {
  if(ptr) {
    delete ptr;
    ptr = nullptr;
  }
}

/***********************************************************************//**
  @method :  SafeArrayDelete
  @comment:  safe delete operation to delete array, ptr is set to 0
  @param  :  ptr pointer to delete
***************************************************************************/
template <typename Type>
void SafeArrayDelete(Type *&ptr) {
  if(ptr) {
    delete[] ptr;
    ptr = nullptr;
  }
}

};
#endif
/* CTYPES_H */

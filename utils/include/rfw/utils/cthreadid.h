/***************************************************************************
    begin                : 2016
    copyright            : (C) 2016 by mry
    email                : mry@hispeed.ch
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#ifndef CTHREADID_H
#define CTHREADID_H

namespace utils
{

/***********************************************************************//**
  @class  : CThreadId
  @package: Utils
***************************************************************************/
class CThreadId
{
private:
  CThreadId(){};
  virtual ~CThreadId(){};

public:
  enum
  {
    THREAD_FIRST = 0
  };

};
};
#endif
/* CTHREADID_H */

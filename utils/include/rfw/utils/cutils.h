/***************************************************************************
    begin                : 2016
    copyright            : (C) 2016 by mry
    email                : mry@hispeed.ch
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#ifndef CUTILS_H_
#define CUTILS_H_

#include "rfw/utils/ctypes.h"

namespace utils
{

/***********************************************************************//**
  @class  : CUtils
  @package: Utils
***************************************************************************/
class CUtils
{
  /// this class can not be copied
  UNCOPYABLE(CUtils);

public:
  static void WaitForTermination();

private:
  CUtils() = delete;
  ~CUtils() = delete;

};
};
#endif
/* CUTILS_H_ */

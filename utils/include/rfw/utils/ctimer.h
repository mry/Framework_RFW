/***************************************************************************
    begin                : 2016
    copyright            : (C) 2016 by mry
    email                : mry@hispeed.ch
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#ifndef CTIMER_H
#define CTIMER_H

#include <time.h>

#include "rfw/utils/ctypes.h"

namespace utils
{
/***********************************************************************//**
  @class  : CTimer
  @package: Utils
***************************************************************************/
class CTimer
{
public:

  typedef enum {
    ESun = 0,
    EMon = 1,
    ETue = 2,
    EWed = 3,
    EThu = 4,
    EFri = 5,
    ESat = 6
  } EUCSIDay;

  typedef enum {
    EJan = 1,
    EFeb = 2,
    EMar = 3,
    EApr = 4,
    EMay = 5,
    EJun = 6,
    EJul = 7,
    EAug = 8,
    ESep = 9,
    EOct = 10,
    ENov = 11,
    EDec = 12
  } EUCSIMonth;


public:
  static int64_t GetNow();

  static void GetNow(
      int32_t& pYear,
      EUCSIMonth& pMonth,
      int32_t& pDay,
      int32_t& pHour,
      int32_t& pMinute,
      int32_t& pSecond);

  static void GetNow(
      EUCSIDay& pDay,
      int32_t& pHour,
      int32_t& pMinute,
      int32_t& pSecond);

  static int64_t GetTimeStamp(
      int32_t pYear,
      EUCSIMonth pMonth,
      int32_t pDay,
      int32_t pHour,
      int32_t pMinute,
      int32_t pSecond);

public:
  CTimer();
  ~CTimer();
  void Start();
  void Stop();
  void Reset(bool restart);
  int32_t GetTime();
  bool IsRunning() { return m_bIsRunning; }
  bool IsReset()   { return m_bIsReset;   }

private:
  time_t m_tStartTime;
  time_t m_tStopTime;
  bool m_bIsReset;
  bool m_bIsRunning;
};
};
#endif
/* CTIMER_H */

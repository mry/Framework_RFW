/***************************************************************************
    begin                : 2016
    copyright            : (C) 2016 by mry
    email                : mry@hispeed.ch
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#ifndef CLOGGER_H_
#define CLOGGER_H_

/***************************************************************************/
/// enable DEBUG mode. If not defined DBG1, DBG2 and INFO are disabled
#define DEBUG_ENABLE 1

#include "rfw/utils/ctypes.h"
#include <iostream>
#include <sstream>

#include <cassert>
#include <mutex>
#include <string>

namespace utils
{


typedef enum
{
  LOG_EMERG_e   = 0,  /* system is unusable */
  LOG_ALERT_e   = 1,  /* action must be taken immediately */
  LOG_CRIT_e    = 2,  /* critical conditions */
  LOG_ERR_e     = 3,  /* error conditions */
  LOG_WARNING_e = 4,  /* warning conditions */
  LOG_NOTICE_e  = 5,  /* normal but significant condition */
  LOG_INFO_e    = 6,  /* informational */
  LOG_DEBUG_e   = 7,  /* debug-level messages */
  LOG_DEBUG1_e  = 8,  /* debug-level messages */
  LOG_DEBUG2_e  = 9,
  LOG_LEVEL_MAX_e = LOG_DEBUG2_e + 1
} LOG_LEVEL_e;
};


/***************************************************************************/
// defines for logging
#ifdef DEBUG_ENABLE

#define DEBUG2(str)  \
{ \
  utils::CLogger::GetInstance().Debug(utils::LOG_DEBUG2_e,__FILE__,__LINE__, \
    dynamic_cast<std::ostringstream&>(std::ostringstream().flush() << str)); \
  };

#define DEBUG1(str)  \
{ \
  utils::CLogger::GetInstance().Debug(utils::LOG_DEBUG1_e,__FILE__,__LINE__, \
    dynamic_cast<std::ostringstream&>(std::ostringstream().flush() << str)); \
  };

#define DEBUG(str)  \
{ \
  utils::CLogger::GetInstance().Debug(utils::LOG_DEBUG_e,__FILE__,__LINE__, \
    dynamic_cast<std::ostringstream&>(std::ostringstream().flush() << str)); \
  };
#else
#define DEBUG2(str) {};
#define DEBUG1(str) {};
#define DEBUG(str) {};
#endif


#define INFO(str)  \
{ \
  utils::CLogger::GetInstance().Debug(utils::LOG_INFO_e,__FILE__,__LINE__, \
    dynamic_cast<std::ostringstream&>(std::ostringstream().flush() << str)); \
  };

#define NOTICE(str)  \
{ \
  utils::CLogger::GetInstance().Debug(utils::LOG_NOTICE_e,__FILE__,__LINE__, \
    dynamic_cast<std::ostringstream&>(std::ostringstream().flush() << str)); \
  };

#define WARN(str)  \
{ \
  utils::CLogger::GetInstance().Debug(utils::LOG_WARNING_e,__FILE__,__LINE__, \
    dynamic_cast<std::ostringstream&>(std::ostringstream().flush() << str)); \
  };

#define ERR(str)  \
{ \
  utils::CLogger::GetInstance().Debug(utils::LOG_ERR_e,__FILE__,__LINE__, \
    dynamic_cast<std::ostringstream&>(std::ostringstream().flush() << str)); \
  };

#define CRITICAL(str)  \
{ \
  utils::CLogger::GetInstance().Debug(utils::LOG_CRIT_e,__FILE__,__LINE__, \
    dynamic_cast<std::ostringstream&>(std::ostringstream().flush() << str)); \
  };

#define ALERT(str)  \
{ \
  utils::CLogger::GetInstance().Debug(utils::LOG_ALERT_e,__FILE__,__LINE__, \
    dynamic_cast<std::ostringstream&>(std::ostringstream().flush() << str)); \
  };

#define EMERG(str)  \
{ \
  utils::CLogger::GetInstance().Debug(utils::LOG_EMERG_e,__FILE__,__LINE__, \
    dynamic_cast<std::ostringstream&>(std::ostringstream().flush() << str)); \
  };

namespace utils
{

/***************************************************************************/

/***********************************************************************//**
  @class  : CLogger
  @package: Utils
***************************************************************************/
class CLogger
{
    /// this class can not be copied
    UNCOPYABLE(CLogger);
  public:
    static CLogger & GetInstance();
    void Debug(LOG_LEVEL_e level, 
              std::string const & module,
              int32_t line, 
              std::ostringstream& str);

    void SetDebugLevel(uint16_t level);

    ~CLogger();
  private:
    CLogger();
    LOG_LEVEL_e m_eLogLevel;
    std::mutex m_sMutex;
};
};
#endif
/* CLogger_H_ */

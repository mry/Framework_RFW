/***************************************************************************
    begin                : 2016
    copyright            : (C) 2016 by mry
    email                : mry@hispeed.ch
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#include "rfw/utils/clogger.h"
#include "rfw/utils/cmessage.h"
#include "rfw/utils/cthread.h"
#include "rfw/utils/cthreadmanager.h"

#include <assert.h>
#include <iostream>
#include <sstream>
#include <stdio.h>
#include <unistd.h>

using namespace std;

namespace utils
{
/***********************************************************************//**
  @method :  CThread
  @comment:  constructor
  @param  :  threadId identifier of thread
  @return :  -
***************************************************************************/
CThread::CThread(uint32_t threadId)
: m_bRunning(false)
, m_nThreadId(threadId)
{
  // register thread
  CThreadManager::GetInstance().RegisterThread(m_nThreadId, this);
}

/***********************************************************************//**
  @method :  ~CThread
  @comment:  destructor
***************************************************************************/
CThread::~CThread()
{
  CThreadManager::GetInstance().DeregisterThread(m_nThreadId);
  if (m_bRunning){
    StopThread();
  }

  // check message queue!
  if (!m_listMessageQueue.empty()){
    ERR("Message Queue not empty: ThreadId: " << m_nThreadId 
        << " number of pending messages: " << m_listMessageQueue.size());
  }

  // delete pending messages
  while(GetPendingMessages()) {
    smartMessagePtr pMsg = ReceiveMessage();
    INFO("consume message: " << pMsg->ToString());
  }
  DEBUG2("Destroy Thread");
}

/***********************************************************************//**
  @method :  GetThreadId
  @comment:  Get Thread ID
  @return :  thread id
***************************************************************************/
uint32_t CThread::GetThreadId()
{
  return m_nThreadId;
}


/***************************************************************************
** Example for run method
*/

/***********************************************************************//**
  @method : Run
  @comment: run method as a template
  @param  : args input argument to cast to wathever
  @return : void * cast to ...
***************************************************************************/
/*
void * CThread::Run(void * args)
{
  sleep(1);
  while(IsRunning()){
    sleep(1);
    if (IsRunning()){
      DEBUG(1,"run: bRunning is true\n");
    }else{
      DEBUG(1,"run: bRunning is false\n");
    }
  }
  return nullptr;
}
*/

/***********************************************************************//**
  @method : StartThread
  @comment: start thread
  @param  : -
  @return : true successfully started
***************************************************************************/
bool CThread::StartThread()
{
  if (!m_bRunning) {
    m_bRunning = true;
    m_workerThread = std::thread([&](){
      while(m_bRunning) {
        Run(nullptr);
      }
      DEBUG2("Terminating...");
    });
  }
  return true;
}

/***********************************************************************//**
  @method : StopThread
  @comment: stop thread
  @param  : -
  @return : true successfully stopped
***************************************************************************/
bool CThread::StopThread()
{
  if (m_bRunning) {
    // stop thread
    m_bRunning = false;

    if (m_workerThread.joinable()) {
      DEBUG2("joining thread");
      m_workerThread.join();
    }

    return true;
  }
  return false;
}

/***********************************************************************//**
  @method : SetThreadPriority
  @comment: set the thread priority
  @param  : priority of thread
  @return : true priority successfully written
***************************************************************************/
bool CThread::SetThreadPriority(int prio)
{
  return false;
}

/***********************************************************************//**
  @method : GetThreadPriority
  @comment: get the thread priority
  @param  : priority of thread
  @return : true priority successfully read
***************************************************************************/
bool CThread::GetThreadPriority(int32_t &priority)
{
  return false;
}

/***********************************************************************//**
  @method : GetPendingMessages
  @comment: get number of pending messages
  @param  : -
  @return : number of messages
***************************************************************************/
uint32_t CThread::GetPendingMessages()
{
  return m_listMessageQueue.size();
}

/***********************************************************************//**
  @method : SendTo
  @comment: send message to thread with given id.
            Add the sender id to the message
  @param  : threadId receiver thread id
  @param  : pMessage pointer to message
  @return : number of messages
***************************************************************************/
bool CThread::SendTo (uint32_t threadId, smartMessagePtr pMessage)
{
  return CThreadManager::GetInstance().Send(GetThreadId(), threadId, pMessage);
}

/***********************************************************************//**
  @method : TransmitMessage
  @comment: transmit a message
  @param  : pMsg pointer to message
  @return : -
***************************************************************************/
void CThread::TransmitMessage(smartMessagePtr pMsg)
{
  {
    std::unique_lock<mutex> lock(m_sMutex);
    m_listMessageQueue.push_front(pMsg);
    SetEvent();
  }
  DEBUG1("transmit Message");
}

/***********************************************************************//**
  @method : ReceiveEvent
  @comment: blocks till message is received
  @param  : -
  @return : Pointer to CMessage
***************************************************************************/
smartMessagePtr CThread::ReceiveMessage()
{
  smartMessagePtr pMsg;
  int32_t event = -1;
  do {
    event = WaitEvent();
    if (event==0){
      // lock and get message
      std::unique_lock<mutex> lock(m_sMutex);
      if (!m_listMessageQueue.empty()){
        pMsg = m_listMessageQueue.back();
        m_listMessageQueue.pop_back();
      }else{
        DEBUG2("message is NULL!!");
      }
    }else{
      ERR("event is not 0");
    }

  } while (event != 0);
  return pMsg;
}

/***********************************************************************//**
  @method : SetEvent
  @comment: trigger an event
  @param  : -
  @return : -
***************************************************************************/
void CThread::SetEvent()
{
  m_sSemaphore.SignalEvent();
}

/***********************************************************************//**
  @method :  WaitEvent
  @comment:  blocking wait for an event
  @param  : -
  @return : result
***************************************************************************/
int32_t CThread::WaitEvent()
{
  // Wait for the event be signaled
  // Infinite wait
  int32_t result = m_sSemaphore.WaitEvent();
  DEBUG2("waitEvent");
  return result;
}
}
/* END_MODULE */

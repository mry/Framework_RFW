/***************************************************************************
    begin                : 2016
    copyright            : (C) 2016 by mry
    email                : mry@hispeed.ch
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#include "rfw/utils/cmessage.h"
#include "rfw/utils/clogger.h"
#include "rfw/utils/csemaphore.h"

namespace utils
{

// initialize static member
uint32_t CMessage::m_nExistingMessages = 0;
std::mutex CMessage::m_MessageCtrMutex;

/***********************************************************************//**
  @method :  CMessage
  @comment:  constructor
***************************************************************************/
CMessage::CMessage()
: m_nSenderThreadId(0)
, m_pSema(0)
{
  DEBUG2("Message++");
  std::unique_lock<std::mutex> lock(m_MessageCtrMutex);
  m_nExistingMessages++;
}

/***********************************************************************//**
  @method :  ~CMessage
  @comment:   destructor
***************************************************************************/
CMessage::~CMessage()
{
  DEBUG2("Message--");

  if (m_pSema)
  {
    m_pSema->SignalEvent();
  }

  {
    std::unique_lock<std::mutex> lock(m_MessageCtrMutex);
    m_nExistingMessages--;
  }
}

/***********************************************************************//**
  @method :  GetNumberOfExistingMessages
  @comment:  get the number of existing messages
  @param  : -
  @return :  number of messages
***************************************************************************/
uint32_t CMessage::GetNumberOfExistingMessages()
{
  return m_nExistingMessages;
}

/***********************************************************************//**
  @method :  SetSemaphore
  @comment:  Set a Semaphore. It will be released, when message is destroyed.
  @param  :  pSema
***************************************************************************/
void CMessage::SetSemaphore( CSemaphore* pSema)
{
  m_pSema = pSema;
}

}
/* END_MODULE */

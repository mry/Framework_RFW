/***************************************************************************
    begin                : 2016
    copyright            : (C) 2016 by mry
    email                : mry@hispeed.ch
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#include "rfw/utils/cmessagebusreactor.h"
#include "rfw/utils/clogger.h"

namespace utils
{
/***********************************************************************//**
 @method :  CMessageBusReactor
 @comment:  constructor
 @param  :  threadId identifier of the thread
 @return :  -
 ***************************************************************************/
CMessageBusReactor::CMessageBusReactor(uint32_t threadId) 
: CThread(threadId)
{
}

/***********************************************************************//**
 @method :  ~CMessageBusReactor
 @comment:  destructor
 ***************************************************************************/
CMessageBusReactor::~CMessageBusReactor()
{
}

/***********************************************************************//**
 @method :  AddHandlerFunction
 @comment:  add a handler function (see definition of handlerFunction)
 @param  :  messageId
 @param  :  memberFnc function of type std::function <void(CMessage*)>
 @return :  -
 ***************************************************************************/
void CMessageBusReactor::AddHandlerFunction(int32_t messageId, 
                                            handlerFunction memberFnc) 
{
  m_Functions.insert(MessageHandlerMap::value_type(messageId, memberFnc));
}

/***********************************************************************//**
 @method : Run
 @comment: run method
 @param  : args input argument to cast to whatever
 @return : void * cast to ...
 ***************************************************************************/
void * CMessageBusReactor::Run(void * args) 
{
  while (IsRunning()) {
    if (IsRunning()) {
      DEBUG2("run: bRunning is true\n");
      ReceiveAndHandleMsg();
    } else {
      DEBUG2("run: bRunning is false\n");
    }
  }
  return nullptr;
}

/***********************************************************************//**
 @method : receiveAndHandleMsg
 @comment: reactor element of the class
 Receive a message and process it accordingly
 @return : -
 ***************************************************************************/
void CMessageBusReactor::ReceiveAndHandleMsg() 
{
  smartMessagePtr pMsg = CThread::ReceiveMessage();

  // find handler
  MessageHandlerMapIterator iter;
  iter = m_Functions.find(pMsg->GetMessageId());
  if (iter == m_Functions.end()) {
    ERR("No Handler found");
  } else {
    handlerFunction func = (*iter).second;
    func(pMsg);
  }
}

}
/* END_MODULE */


/***************************************************************************
                          cconverter.cpp  -  description
                             -------------------
    begin                : Tue Oct 11 2016
    copyright            : (C) 2016 by mry
    email                : mry@hispeed.ch

 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#include "rfw/utils/cconverter.h"

#include <sys/stat.h>
#include <fcntl.h>
#include <unistd.h>
#include <cstring>
#include <cstdio>
#include <cassert>
#include <limits>
#include <stdexcept>
#include <sstream>
#include <iomanip>

namespace utils
{
namespace converter
{

/***********************************************************************//**
  @method : trim
  @comment: trim a file. remote \t \n \r from end
  @param  :  _str input string
  @return : trimmed string
***************************************************************************/
std::string trim(const std::string& _str) 
{
  std::string result = _str;
  std::string::size_type notwhite = result.find_first_not_of( " \t\n\r" );
  result.erase(0,notwhite);
  notwhite = result.find_last_not_of( " \t\n\r" );
  result.erase( notwhite + 1 );
  return result;
} // trim

/***********************************************************************//**
  @method : strToInt
  @comment: convert a string to an integer.
            can throw invalid argument exception
  @param  : _strValue input string value
  @return : converted value as int
***************************************************************************/
int32_t strToInt(const std::string& _strValue) 
{
  if(!_strValue.empty()) {
    char* endp;
    int result = strtol(_strValue.c_str(), &endp, 0);
    if(*endp == '\0') {
      return result;
    }
  }
  throw std::invalid_argument(std::string("strToInt: Could not parse value: '") + _strValue + "'");
} // strToInt

/***********************************************************************//**
  @method : strToIntDef
  @comment: convert a string to an integer.
            If conversion not possible, return default value
  @param  : _strValue input string value
  @param  : _default default value
  @return : converted value as int
***************************************************************************/
int32_t strToIntDef(const std::string& _strValue, const int32_t _default) 
{
  if(!_strValue.empty()) {
    char* endp;
    int result = strtol(_strValue.c_str(), &endp, 0);
    if(*endp == '\0') {
      return result;
    }
  }
  return _default;
} // strToIntDef

/***********************************************************************//**
  @method : strToUInt
  @comment: convert a string to an unsigned integer.
            can throw invalid argument exception
  @param  : _strValue input string value
  @return : converted value as int
***************************************************************************/
uint32_t strToUInt(const std::string& _strValue) 
{
  if(!_strValue.empty()) {
    char* endp;
    uint32_t result = strtoul(_strValue.c_str(), &endp, 0);
    if(*endp == '\0') {
      return result;
    }
  }
  throw std::invalid_argument(std::string("strToUInt: Could not parse value: '") + _strValue + "'");
} // strToUInt

/***********************************************************************//**
  @method : strToUIntDef
  @comment: convert a string to an unsigned integer.
            If conversion not possible, return default value
  @param  : _strValue input string value
  @param  : _default default value
  @return : converted value as unsigned int
***************************************************************************/
uint32_t strToUIntDef(const std::string& _strValue, const uint32_t _default) 
{
  if(!_strValue.empty()) {
    char* endp;
    uint32_t result = strtoul(_strValue.c_str(), &endp, 0);
    if(*endp == '\0') {
      return result;
    }
  }
  return _default;
} // strToUIntDef


/***********************************************************************//**
  @method : strToULongLong
  @comment: convert a string to an unsigned long long
            can throw invalid argument exception
  @param  : _strValue input string value
  @return : converted value as unsigned long long
***************************************************************************/
uint64_t strToULongLong(const std::string& _strValue) 
{
  if(!_strValue.empty()) {
    char* endp;
    uint64_t result = strtoull(_strValue.c_str(), &endp, 0);
    if(*endp == '\0') {
      return result;
    }
  }
  throw std::invalid_argument(std::string("strToUInt: Could not parse value: '") + _strValue + "'");
} // strToULongLong

/***********************************************************************//**
  @method : strToULongLongDef
  @comment: convert a string to an unsigned long long.
            If conversion not possible, return default value
  @param  : _strValue input string value
  @param  : _default default value
  @return : converted value as unsigned long long
***************************************************************************/
uint64_t strToULongLongDef(const std::string& _strValue, const unsigned int _default) 
{
  if(!_strValue.empty()) {
    char* endp;
    uint64_t result = strtoull(_strValue.c_str(), &endp, 0);
    if(*endp == '\0') {
      return result;
    }
  }
  return _default;
} // strToULongLongDef

/***********************************************************************//**
  @method : strToDouble
  @comment: convert a string to a double
            can throw invalid argument exception
  @param  : _strValue input string value
  @return : converted value as double
***************************************************************************/
double strToDouble(const std::string& _strValue) 
{
  if(!_strValue.empty()) {
    char* endp;
    double result = strtod(_strValue.c_str(), &endp);
    if(*endp == '\0') {
      return result;
    }
  }
  throw std::invalid_argument(std::string("strToDouble: Could not parse value: '") + _strValue + "'");
} // strToDouble


/***********************************************************************//**
  @method : strToDoubleDef
  @comment: convert a string to a double
            If conversion not possible, return default value
  @param  : _strValue input string value
  @param  : _default default value
  @return : converted value as double
***************************************************************************/
double strToDoubleDef(const std::string& _strValue, const double _default) 
{
  if(!_strValue.empty()) {
    char* endp;
    double result = strtod(_strValue.c_str(), &endp);
    if(*endp == '\0') {
      return result;
    }
  }
  return _default;
} // strToDouble

/***********************************************************************//**
  @method doubleToString
  @comment: convert a double to a string
  @param  : _value input value
  @return : converted value as string
***************************************************************************/
std::string doubleToString(const double _value) 
{
  std::stringstream sstream;
  sstream << std::setprecision(12) << _value;
  return sstream.str();
} // doubleToString

/***********************************************************************//**
  @method : intToString
  @comment: convert a integer to a string (hex or dec based)
  @param  : _int input value
  @param  : _hex boolean flag to convert to hex to decimal
  @return : converted value as string
***************************************************************************/
std::string intToString(const uint64_t _int, bool _hex) 
{
  //
  // http://en.cppreference.com/w/cpp/types/numeric_limits/digits10
  // ... any number with this many decimal digits can be converted to a value
  // of type T and back to decimal form, without change due to rounding or
  // overflow.
  // It's a lower bound, the type may also hold numbers with one digit more
  // but not all of them
  // +3 for sign, '\0' terminator and upper bound
  //
  const int max_size = std::numeric_limits<long long>::digits10 + 3;
  char buffer[max_size] = { 0 };
  int32_t n;
  if (_hex) {
    n = snprintf(buffer, max_size, "0x%llx", _int);
  } else {
    n = snprintf(buffer, max_size, "%lld", _int);
  }
  assert(n < max_size);
  return std::string(buffer);
} // intToString

/***********************************************************************//**
  @method : uintToString
  @comment: convert an unsigned long long to a string (hex or dec based)
  @param  : _int input value
  @param  : _hex boolean flag to convert to hex to decimal
  @return : converted value as string
***************************************************************************/
std::string uintToString(uint64_t _int, bool _hex) 
{
  // +2 for '\0' terminator and upper bound
  const int max_size = std::numeric_limits<uint64_t>::digits10 + 2;
  char buffer[max_size] = { 0 };
  int n;
  if (_hex) {
    n = snprintf(buffer, max_size, "0x%llx", _int);
  } else {
    n = snprintf(buffer, max_size, "%llu", _int);
  }
  assert(n < max_size);
  return std::string(buffer);
} // uintToString

/***********************************************************************//**
  @method : hexEncodeByteArray
  @comment: convert an unsigned char array of given length to hex encoded string
  @param  : a pointer to unsigned char array
  @param  : len length of the array
  @return : converted value as string
***************************************************************************/
std::string hexEncodeByteArray(const unsigned char *a, uint32_t len) 
{
  std::ostringstream s;
  s << std::hex;
  s.fill('0');
  for (unsigned i = 0; i < len; i++) {
    s.width(2);
    s << static_cast<unsigned int>(a[i] & 0x0ff);
  }
  return s.str();
}

/***********************************************************************//**
  @method : unsignedLongIntToHexString
  @comment: convert an unsigned long long to a hex string
  @param  : _value input value
  @return : converted value as string
***************************************************************************/
std::string unsignedLongIntToHexString(const uint64_t _value) 
{
  std::stringstream sstream;
  sstream << std::hex << _value;
  return sstream.str();
}

/***********************************************************************//**
  @method : unsignedLongIntToString
  @comment: convert an unsigned long long to a decimal string
  @param  : _value input value
  @return : converted value as string
***************************************************************************/
std::string unsignedLongIntToString(const uint64_t _value) 
{
  std::stringstream sstream;
  sstream << _value;
  return sstream.str();
}

/***********************************************************************//**
  @method : splitString
  @comment: split a input string at given _delimiter signs and trim, if needded
  @param  : _source input string
  @param  : _delimiter delimiting character to split the string
  @param  : _TrimEntries trim the string
  @return : vector of split strings
***************************************************************************/
std::vector<std::string> splitString(const std::string& _source,
                                     const char _delimiter,
                                     bool _trimEntries) 
{
  std::vector<std::string> result;
  std::string curString = _source;
  std::string::size_type delimPos = 0, startField = 0, skip = 0;

  while (startField < curString.size()) {
    delimPos = curString.find(_delimiter, skip);

    if ((delimPos > 0) && (delimPos != std::string::npos)) {
      if (curString.at(delimPos - 1) == '\\') {
        // remove escape character
        curString.erase(delimPos - 1, 1);
        skip = delimPos;
        continue;
      }
    }

    if (delimPos != std::string::npos) {
      if (_trimEntries) {
        result.push_back(trim(curString.substr(startField, delimPos - startField)));
      } else {
        result.push_back(curString.substr(startField, delimPos - startField));
      }
      skip = startField = delimPos + 1;
      if (curString.size() - skip == 0) {
        // in case of trailing delimiter, but no actual field data, we add an
        // empty string. not sure why we need this, probably nobody does
        result.push_back("");
      }
    } else {
      if (_trimEntries) {
        result.push_back(trim(curString.substr(startField)));
      } else {
        result.push_back(curString.substr(startField));
      }
      break;
    }
  }
  return result;
} // splitString
}
}

/***************************************************************************
    begin                : 2016
    copyright            : (C) 2016 by mry
    email                : mry@hispeed.ch
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#include "rfw/utils/ctimermessage.h"
#include "rfw/utils/clogger.h"

#include <iostream>

namespace utils
{
/***********************************************************************//**
  @method : CTimerMessage
  @comment: constructor
***************************************************************************/
CTimerMessage::CTimerMessage()
{
}

/***********************************************************************//**
  @method : ~CTimerMessage
  @comment: destructor
***************************************************************************/
CTimerMessage::~CTimerMessage()
{
}

/***********************************************************************//**
  @method : GetMessageId
  @comment: get message identifier
  @param  : -
  @return : the identifier
***************************************************************************/
int32_t CTimerMessage::GetMessageId() const
{
  return EVENT_TIMER_e;
}

/***********************************************************************//**
  @method :  ToString
  @comment:  logging operation to display state
  @param  : -
  @return : string
***************************************************************************/
std::string CTimerMessage::ToString() const
{
  std::string output = "CTimerMessage";
  return output;
}
}
/* END_MODULE */

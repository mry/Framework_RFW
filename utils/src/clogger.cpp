/***************************************************************************
    begin                : 2016
    copyright            : (C) 2016 by mry
    email                : mry@hispeed.ch
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#include "rfw/utils/clogger.h"
#include "rfw/utils/cdatetools.h"
#include "rfw/utils/cconverter.h"

#include <iostream>
#include <vector>
#include <sstream>
#include <syslog.h>

using namespace std;

namespace utils
{
/***********************************************************************//**
  @method :  CLogger
  @comment:  constructor
***************************************************************************/
CLogger::CLogger()
: m_eLogLevel{LOG_ERR_e}
{
}

/***********************************************************************//**
  @method :  ~CLogger
  @comment:  destructor
***************************************************************************/
CLogger::~CLogger()
{
}

/***********************************************************************//**
  @method :  getInstance
  @comment:  static operation to get instance of class
  @param  : -
  @return :  Instance of class
***************************************************************************/
CLogger & CLogger::GetInstance()
{
  static CLogger m_sLogger;
  return m_sLogger;
}

/***********************************************************************//**
  @method :  setDebugLevel
  @comment:  set the level for debugging
  @param  :  level debug level
  @return :  -
***************************************************************************/
void CLogger::SetDebugLevel(uint16_t level)
{
  if (level > LOG_DEBUG2_e) {
    level = LOG_DEBUG2_e;
  }
  m_eLogLevel = (LOG_LEVEL_e)level;
  cout << "LogLevel set to:" << m_eLogLevel << endl;
}

/***********************************************************************//**
  @method :  pDebug
  @comment:  log operation
  @param  :  level level of debug message
  @param  :  module module name
  @param  :  line line of message origin
  @param  :  str data of message
  @return :  -
***************************************************************************/
void CLogger::Debug(LOG_LEVEL_e level, 
                    string const & module,
                    int32_t line, 
                    ostringstream& str)
{
  if (level <= m_eLogLevel){
    std::unique_lock<std::mutex> lock(m_sMutex);
    ostringstream output;

    CDateTime now;
    output << now.toISO8601_ms_local();

    int32_t logLevel = LOG_DEBUG;
    switch (level){
      case LOG_EMERG_e   : output << "EMRG: "; logLevel = LOG_EMERG;    break;
      case LOG_ALERT_e   : output << "ALRT: "; logLevel = LOG_ALERT;    break;
      case LOG_CRIT_e    : output << "CRIT: "; logLevel = LOG_CRIT;     break;
      case LOG_ERR_e     : output << "ERRR: "; logLevel = LOG_PERROR;   break;
      case LOG_WARNING_e : output << "WARN: "; logLevel = LOG_WARNING;  break;
      case LOG_NOTICE_e  : output << "NOTI: "; logLevel = LOG_NOTICE;   break;
      case LOG_INFO_e    : output << "INFO: "; logLevel = LOG_INFO;     break;
      case LOG_DEBUG_e   : output << "DBG0: "; logLevel = LOG_DEBUG;    break;
      case LOG_DEBUG1_e  : output << "DBG1: "; logLevel = LOG_DEBUG;    break;
      case LOG_DEBUG2_e  : output << "DBG2: "; logLevel = LOG_DEBUG;    break;
    }

    std::vector<std::string> vec = converter::splitString(module, '/', true);
    if (vec.size() > 1) {
      std::string out = vec[vec.size()-1];
      output << out << ":" << line << "->" << str.str();
    } else {
      output << module << ":" << line << "->" << str.str();
    }

    const string& tmp = output.str();
    const char* cstr = tmp.c_str();
    ::syslog(logLevel, "%s", cstr);

    cout << output.str() << endl;
  }
}
}
/* END_MODULE */

/***************************************************************************
    begin                : 2017
    copyright            : (C) 2017 by mry
    email                : mry@hispeed.ch
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#include "rfw/utils/cdatetools.h"

#include <libical/ical.h>
#include <cstring>
#include <stdexcept>
#include <errno.h>
#include <math.h>
#include <sys/time.h>

namespace utils {

  /*
   * TODO can DateTools bre replaced with this:
   * http://www.boost.org/doc/libs/1_55_0/doc/html/date_time/details.html#date_time.tradeoffs
   */

  /**
   * Converts \tm to local timezone of this machine
   * @tm contains '2009-01-02T15:00:00+0300'
   * @tz_offset seconds east of utc
   * @ret struct tm
   */
  static time_t convertTZ(struct tm tm, time_t tz_offset) 
  {
    time_t t0;

    /*
     * mktime will override tm_gmtoff with offset of local
     * timezone. tm_isdst can be 1, 0, -1 aka enabled,
     * disabled and figure it out
     * http://caml.inria.fr/mantis/print_bug_page.php?bug_id=1882
     */
    tm.tm_isdst = -1; /* rely on mktime for daylight saving time */
    t0 = mktime(&tm);
    if (t0 == -1) {
      throw std::invalid_argument("mktime");
    }

    /*
     * mktime did not modify year, month, day, hour, minutes
     * and second. It has only overwritten the timezone and
     * daylight saving time, which is of course incorrect
     * without also correcting the hours.
     * NOTE: the input tz_offset includes daylight saving
     * time correction, the tm_gmtoff computed by mktime as well
     * By subracting the old offset we get UTC by adding
     * the local offset we get localtime
     */
    t0 -= (tz_offset - tm.tm_gmtoff);
    return t0;
  }

  /**
   * TODO icaltime_as_timet sufficient?
   */
  static time_t ical_to_tm(const icaltimetype& icalTime) 
  {
    time_t t0;
    struct tm tm;
    memset(&tm, '\0', sizeof(tm));
    if(!icalTime.is_date) {
        tm.tm_sec = icalTime.second;
        tm.tm_min = icalTime.minute;
        tm.tm_hour = icalTime.hour;
    }
    tm.tm_mday = icalTime.day;
    tm.tm_mon = icalTime.month - 1;
    tm.tm_year = icalTime.year - 1900;
    tm.tm_isdst = -1;
    t0 = mktime(&tm);
    if (t0 == -1) {
      throw std::invalid_argument("mktime");
    }
    return t0;
  } // ical_to_tm

  //================================================== CDateTime

  CDateTime::CDateTime() 
  {
    if (gettimeofday(&m_timeval, nullptr)) {
      throw std::runtime_error(strerror(errno));
    }
  } // ctor

  CDateTime::CDateTime(time_t secs, suseconds_t usec) 
  {
    m_timeval.tv_sec = secs;
    m_timeval.tv_usec = usec;
  } // ctor(time_t)

  CDateTime::CDateTime(const CDateTime& _copy)
  : m_timeval(_copy.m_timeval)
  { 
  } // ctor(copy)

  CDateTime CDateTime::addHour(const int _hours) const 
  {
    struct timeval tv = m_timeval;
    tv.tv_sec += _hours * 60 * 60;
    return CDateTime(tv);
  } // addHour

  CDateTime CDateTime::addMinute(const int _minutes) const 
  {
    struct timeval tv = m_timeval;
    tv.tv_sec += _minutes * 60;
    return CDateTime(tv);
  } // addMinute

  CDateTime CDateTime::addSeconds(const int _seconds) const 
  {
    struct timeval tv = m_timeval;
    tv.tv_sec += _seconds;
    return CDateTime(tv);
  } // addSeconds

  CDateTime CDateTime::addMonth(const int _month) const 
  {
    struct timeval tv = m_timeval;
    struct tm tm;

    localtime_r(&m_timeval.tv_sec, &tm);
    tm.tm_mon += _month;
    tv.tv_sec = mktime(&tm);
    return CDateTime(tv);
  } // addMonth

  CDateTime CDateTime::addYear(const int _years) const 
  {
    struct timeval tv = m_timeval;
    struct tm tm;

    localtime_r(&m_timeval.tv_sec, &tm);
    tm.tm_year += _years;
    tv.tv_sec = mktime(&tm);
    return CDateTime(tv);
  } // addYear

  CDateTime CDateTime::addDay(const int _days) const 
  {
    struct timeval tv = m_timeval;
    tv.tv_sec += _days * 24 * 60 * 60;
    return CDateTime(tv);
  } // addDay

  int CDateTime::getDay() const 
  {
    struct tm tm;
    localtime_r(&m_timeval.tv_sec, &tm);
    return tm.tm_mday;
  } // getDay

  int CDateTime::getMonth() const 
  {
    struct tm tm;
    localtime_r(&m_timeval.tv_sec, &tm);
    return tm.tm_mon;
  } // getMonth

  int CDateTime::getYear() const 
  {
    struct tm tm;
    localtime_r(&m_timeval.tv_sec, &tm);
    return tm.tm_year + 1900;
  } // getYear

  int CDateTime::getHour() const 
  {
    struct tm tm;
    localtime_r(&m_timeval.tv_sec, &tm);
    return tm.tm_hour;
  } // getHour

  int CDateTime::getMinute() const 
  {
    struct tm tm;
    localtime_r(&m_timeval.tv_sec, &tm);
    return tm.tm_min;
  } // getMinute

  int CDateTime::getSecond() const 
  {
    struct tm tm;
    localtime_r(&m_timeval.tv_sec, &tm);
    return tm.tm_sec;
  } // getSecond

  int CDateTime::getDayOfYear() const 
  {
    struct tm tm;
    localtime_r(&m_timeval.tv_sec, &tm);
    return tm.tm_yday;
  } // getDayOfYear

  Weekday CDateTime::getWeekday() const 
  {
    struct tm tm;
    localtime_r(&m_timeval.tv_sec, &tm);
    return (Weekday)tm.tm_wday;
  } // getWeekday

  bool CDateTime::operator==(const CDateTime& _other) const 
  {
    return difference(_other) == 0;
  } // operator==

  bool CDateTime::operator!=(const CDateTime& _other) const 
  {
    return difference(_other) != 0;
  } // operator!=

  bool CDateTime::operator<(const CDateTime& _other) const 
  {
    return difference(_other) < 0;
  } // operator<

  bool CDateTime::operator>(const CDateTime& _other) const 
  {
    return difference(_other) > 0;
  } // operator>

  bool CDateTime::operator<=(const CDateTime& _other) const 
  {
    return difference(_other) <= 0;
  } // operator<=

  bool CDateTime::operator>=(const CDateTime& _other) const 
  {
    return difference(_other) >= 0;
  } // operator>=

  int CDateTime::difference(const CDateTime& _other) const 
  {
    // TODO useconds are ignored
    // TODO use timercmp, timersub
    return static_cast<int>(difftime(m_timeval.tv_sec,
                            _other.m_timeval.tv_sec));
  } // difference

  time_t CDateTime::secondsSinceEpoch() const 
  {
    return m_timeval.tv_sec;
  }

  long int CDateTime::getTimezoneOffset() const 
  {
    struct tm tm;
    localtime_r(&m_timeval.tv_sec, &tm);
    return tm.tm_gmtoff;
  }

  CDateTime::operator std::string() const 
  {
    return toString();
  } // operator std::string()

  std::string CDateTime::toString() const 
  {
    return toPrettyString();
  }

  std::string CDateTime::toRFC2822String() const 
  {
    static const char* theRFC2822FormatString = "%a, %d %b %Y %T %z";
    struct tm tm;
    char buf[32];

    localtime_r(&m_timeval.tv_sec, &tm);
    strftime(buf, sizeof buf, theRFC2822FormatString, &tm);
    return std::string(buf);
  } // toRFC2822String

  std::string CDateTime::toRFC2445IcalDataTime() const 
  {
    struct tm tm;
    char buf[20];

    /*
     * http://www.ietf.org/rfc/rfc2445.txt
     * http://www.kanzaki.com/docs/ical/CDateTime.html
     */
    localtime_r(&m_timeval.tv_sec, &tm);
    strftime(buf, sizeof buf, "%Y%m%dT%H%M%S", &tm);
    return std::string(buf);
  }

  CDateTime CDateTime::parseRFC2445(const std::string& timeString) 
  {
    struct tm tm;
    time_t t0;
    bool utc = timeString[timeString.size() - 1] == 'Z';

    if (timeString.length() != strlen("19930202T220202") + utc ? 1 : 0) {
      throw std::invalid_argument("RFC2445: too short " + timeString);
    }

    memset(&tm, 0, sizeof(tm));
    char *end = strptime(timeString.c_str(), "%Y%m%dT%H%M%S", &tm);
    if (!end || (end - timeString.c_str() != strlen("19930202T226202"))) {
      // strptime %M matches 0-59, so ..T226202 above results in 22:06:20,
      // the trailing '2' is silently dropped
      throw std::invalid_argument("RFC2445: invalid " + timeString);
    }

    tm.tm_isdst = -1;
    t0 = mktime(&tm);
    if (t0 == -1) {
      throw std::invalid_argument("RFC2445: mktime failure" + timeString);
    }
    if (utc) {
      t0 += tm.tm_gmtoff;
    }
    return CDateTime(t0);
  }

  /*
   * ISO 8061
   * http://www.cl.cam.ac.uk/~mgk25/iso-time.html
   * http://www.cs.tut.fi/~jkorpela/iso8601.html
   * http://www.w3.org/TR/NOTE-CDateTime
   * http://www.ietf.org/rfc/rfc3339.txt
   */
  CDateTime CDateTime::parseISO8601(std::string in) 
  {
    bool utc = in[in.size() - 1]  == 'Z';
    struct tm tm;

    if (in.length() != strlen("2011-10-08T07:07:09+02:30") &&
        in.length() != strlen("2011-10-08T07:07:09+0200") &&
        in.length() != strlen("2011-10-08T07:07:09+02:") &&
        in.length() != strlen("2011-10-08T07:07:09+02") &&
        in.length() != strlen("2011-10-08T07:07:09Z")) {
      throw std::invalid_argument("ISO8601: invalid length " + in);
    }

    char *end;
    memset(&tm, 0, sizeof tm);
    if (utc) {
      end = strptime(in.c_str(), "%FT%TZ", &tm);
    } else {
      end = strptime(in.c_str(), "%FT%T%z", &tm);
    }
    if (!end || (*end != '\0' && *end != ':')) {
        throw std::invalid_argument("ISO8601: invalid format " + in);
    }

    if (*end == ':' && *(end + 1) != '\0') {
        // strptime can't parse timezone with colon
        char *tz_minutes = end + 1;
        int ret;

        errno = 0;
        ret = strtod(tz_minutes, &end);
        if (!end || end == tz_minutes || *end != '\0' ||
            (ret == HUGE_VAL || ret == -HUGE_VAL) ||
            (ret == 0 && errno == ERANGE)) {
            throw std::invalid_argument("ISO8601: invalid tz minutes " + in);
        }

        tm.tm_gmtoff += ret * 60; // gmtoff in seconds
    }

    try {
      return CDateTime(convertTZ(tm, tm.tm_gmtoff));
    } catch (std::invalid_argument &e) {
      throw std::invalid_argument(std::string("ISO8601: ") + e.what() + " " + in);
    }
  }

  std::string CDateTime::toISO8601() const 
  {
    struct tm tm;
    char buf[sizeof "2011-10-08T07:07:09Z"];

    gmtime_r(&m_timeval.tv_sec, &tm);
    strftime(buf, sizeof buf, "%FT%TZ", &tm);
    return std::string(buf);
  }

  std::string CDateTime::toISO8601_local() const 
  {
    struct tm tm;
    char buf[sizeof "2011-10-08T07:07:09.000+02:00"];

    localtime_r(&m_timeval.tv_sec, &tm);
    strftime(buf, sizeof buf, "%FT%T%z", &tm);

    /* ensure there is a colon in time zone */
    if (buf[22] != ':') {
        buf[25] = '\0';
        buf[24] = buf[23];
        buf[23] = buf[22];
        buf[22] = ':';
    }
    return std::string(buf);
  }

  /*
   * 100 Hz machine has less than 10ms accuracy
   * http://stackoverflow.com/questions/361363/how-to-measure-time-in-milliseconds-using-ansi-c
   * TODO: is ms really part of 8601
   */
  std::string CDateTime::toISO8601_ms_local() const 
  {
    struct tm tm;
    char buf[sizeof "2011-10-08T07:07:09.000+02:00"];
    char fmt[sizeof "2011-10-08T07:07:09.000+02:00"];

    // http://stackoverflow.com/questions/1551597/using-strftime-in-c-how-can-i-format-time-exactly-like-a-unix-timestamp
    localtime_r(&m_timeval.tv_sec, &tm);
    strftime(fmt , sizeof fmt, "%FT%T.%%03u%z", &tm);
    snprintf(buf, sizeof buf, fmt, m_timeval.tv_usec / 1000);

    /* ensure there is a colon in time zone */
    if (buf[26] != ':') {
        buf[29] = '\0';
        buf[28] = buf[27];
        buf[27] = buf[26];
        buf[26] = ':';
    }
    return std::string(buf);
  }

  /*
   * 100 Hz machine has less than 10ms accuracy
   * http://stackoverflow.com/questions/361363/how-to-measure-time-in-milliseconds-using-ansi-c
   * TODO: is ms really part of 8601
   */
  std::string CDateTime::toISO8601_ms() const 
  {
    struct tm tm;
    char buf[40], fmt[40];

    // http://stackoverflow.com/questions/1551597/using-strftime-in-c-how-can-i-format-time-exactly-like-a-unix-timestamp
    gmtime_r(&m_timeval.tv_sec, &tm);
    strftime(fmt , sizeof fmt, "%FT%T.%%03uZ", &tm);
    snprintf(buf, sizeof buf, fmt, m_timeval.tv_usec / 1000);
    return std::string(buf);
  }

  /*
   * This is no ISO standard
   * http://www.iso.org/iso/home/search.htm?qt=+date+and+time
   * To be ISO 8601 suggests to separate date and time with the letter 'T'
   * also timezone is missing
   */
  CDateTime CDateTime::parsePrettyString(const std::string& timeString) 
  {
    const char* theISOFormatString = "%Y-%m-%d %H:%M:%S"; // 19
    struct tm tm;
    time_t t0;

    memset(&tm, 0, sizeof(tm));
    if (strptime(timeString.c_str(), theISOFormatString, &tm) == nullptr) {
      throw std::invalid_argument("unknown time format: " + timeString);
    }
    tm.tm_isdst = -1;
    t0 = mktime(&tm);
    if (t0 == -1) {
      throw std::invalid_argument("mktime");
    }
    return CDateTime(t0);
  }

  std::string CDateTime::toPrettyString() const 
  {
    struct tm tm;
    char buf[30];
    localtime_r(&m_timeval.tv_sec, &tm);
    strftime(buf, sizeof buf, "%Y-%m-%d %H:%M:%S", &tm);
    return std::string(buf);
  }

  std::ostream& operator<<(std::ostream& out, const CDateTime& _dt) 
  {
    out << _dt.toString();
    return out;
  } // operator<<

  CDateTime CDateTime::NullDate{0};
}


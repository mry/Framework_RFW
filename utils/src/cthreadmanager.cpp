/***************************************************************************
    begin                : 2016
    copyright            : (C) 2016 by mry
    email                : mry@hispeed.ch
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#include "rfw/utils/cthreadmanager.h"
#include "rfw/utils/cthread.h"
#include "rfw/utils/cmessage.h"
#include "rfw/utils/clogger.h"
#include "rfw/utils/csemaphore.h"

#include <cassert>

using namespace std;

namespace utils
{
/***********************************************************************//**
  @method :  CThreadManager
  @comment:  constructor
***************************************************************************/
CThreadManager::CThreadManager()
{
}

/***********************************************************************//**
  @method :  ~CThreadManager
  @comment:  destructor
***************************************************************************/
CThreadManager::~CThreadManager()
{
}

/***********************************************************************//**
  @method :  GetInstance
  @comment:  Get instance of this class (singleton)
  @param:    -
  @return:   instance of thread
***************************************************************************/
CThreadManager& CThreadManager::GetInstance()
{
  static CThreadManager m_instance;
  return  m_instance;
}

/***********************************************************************//**
  @method :  RegisterThread
  @comment:  register thread with thread id
  @param:    threadId id of thread
  @param:    pThread pointer to thread
  @return:  -
***************************************************************************/
void CThreadManager::RegisterThread (uint32_t  threadId, 
                                     CThread * const pThread )
{
  m_mapDictionary.insert(CThreadMap_t::value_type(threadId, pThread));
}

/***********************************************************************//**
  @method :  DeregisterThread
  @comment:  remove thread
  @param:    threadId id of thread
  @return:  -
***************************************************************************/
void CThreadManager::DeregisterThread (uint32_t threadId)
{
  m_mapDictionary.erase(threadId);
}

/***********************************************************************//**
  @method :  GetThread
  @comment:  get thread belonging to thread id
  @param:    threadId id of thread
  @return:   pointer to thread or null
***************************************************************************/
CThread *const CThreadManager::GetThread (uint32_t threadId)
{
  auto mapIter = m_mapDictionary.find(threadId);
  if (mapIter == m_mapDictionary.end()){
    return nullptr;
  }
  return (*mapIter).second;
}

/***********************************************************************//**
  @method :  Send
  @comment:  send message to thread
  @param:    senderId originator thread id
  @param:    receiverId thread id to send to
  @param:    pMessage
  @return:   true successfully sent
             false not successful, receiver thread not found
***************************************************************************/
bool CThreadManager::Send (uint32_t senderId, 
                           uint32_t receiverId, 
                           smartMessagePtr pMessage)
{
  bool retVal = false;
  pMessage->SetSenderId(senderId);
  CThread * const pThread = GetThread(receiverId);
  if (pThread){
    pThread->TransmitMessage(pMessage);
    DEBUG2("message transmitted");
    retVal = true;
  }else{
    ERR("No thread registered");
  }
  return retVal;
}

/***********************************************************************//**
  @method :  Send
  @comment:  send message to thread synchronoulsy. Wait till processed.
  @param:    senderId originator thread id
  @param:    receiverId thread id to send to
  @param:    pMessage
  @return:   true successfully sent
             false not successful, receiver thread not found
***************************************************************************/
bool CThreadManager::SendSync (uint32_t senderId, 
                               uint32_t receiverId, 
                               CMessage* pMessage)
{
  CThread * const pThread = GetThread(receiverId);

  if (!pThread){
    ERR("No thread registered");
    return false;
  } else {

    SMART_PTR(CSemaphore) sema = CREATE_SMART_PTR(CSemaphore);
    {
      smartMessagePtr msg = std::shared_ptr<CMessage>(pMessage);
      msg->SetSenderId(senderId);
      msg->SetSemaphore(sema.get());
      pThread->TransmitMessage(msg);
      DEBUG2("message transmitted, waiting");
    }
    sema->WaitEvent();
    DEBUG2("message transmitted synchronously");
    return true;
  }
}
}
/* END_MODULE */

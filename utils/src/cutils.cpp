/***************************************************************************
    begin                : 2016
    copyright            : (C) 2016 by mry
    email                : mry@hispeed.ch
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#include "rfw/utils/cutils.h"
#include "rfw/utils/ctypes.h"

#include <signal.h>

namespace utils
{
/***********************************************************************//**
  @method :  WaitForTermination
  @comment:  wait for termination signal
***************************************************************************/
void CUtils::WaitForTermination()
{
  /// wait for termination signal
  int32_t signr;
  sigset_t signalSet;
  ::sigemptyset(&signalSet);
  ::sigaddset(&signalSet, SIGINT);
  ::pthread_sigmask(SIG_BLOCK, &signalSet, nullptr);
  ::sigwait(&signalSet, &signr);
}

}
/* END_MODULE */

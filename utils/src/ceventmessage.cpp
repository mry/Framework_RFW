/***************************************************************************
    begin                : 2016
    copyright            : (C) 2016 by mry
    email                : mry@hispeed.ch
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#include "rfw/utils/ceventmessage.h"

#include <sstream>

namespace utils
{

/***********************************************************************//**
  @method :  CEventMessage
  @comment:  constructor
***************************************************************************/
CEventMessage::CEventMessage()
: m_nCommandID{0}
{
}

/***********************************************************************//**
  @method :  CEventMessage
  @comment:  constructor
  @param  :  command command identifier
***************************************************************************/
CEventMessage::CEventMessage(int32_t command)
: m_nCommandID{command}
{
}

/***********************************************************************//**
  @method :  ~CEventMessage
  @comment:
***************************************************************************/
CEventMessage::~CEventMessage()
{
}

/***********************************************************************//**
  @method : GetMessageId
  @comment: get message identifier
  @param  : -
  @return : the identifier
***************************************************************************/
int32_t CEventMessage::GetMessageId() const
{
  return MESSAGE_EVENT_e;
}

/***********************************************************************//**
  @method :  ToString
  @comment:  logging operation to display state
  @param  : -
  @return : string
***************************************************************************/
std::string CEventMessage::ToString() const
{
  std::ostringstream output;
  output << "CEventMessage Command: " << m_nCommandID;
  return output.str();
}

/***********************************************************************//**
  @method : SetCommand
  @comment: set the command id
  @param  : command the command id
  @return : -
***************************************************************************/
void CEventMessage::SetCommand(int32_t command)
{
  m_nCommandID = command;
}

/***********************************************************************//**
  @method :  GetCommand
  @comment:  get the command id
  @param  :  -
  @return : the command id
***************************************************************************/
int32_t CEventMessage::GetCommand() const
{
  return m_nCommandID;
}

}
/* END_MODULE */

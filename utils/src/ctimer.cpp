/***************************************************************************
    begin                : 2016
    copyright            : (C) 2016 by mry
    email                : mry@hispeed.ch
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#include "rfw/utils/ctimer.h"

#include <iostream>
#include <chrono>
#include <stdio.h>

namespace utils
{
/***********************************************************************//**
  @method :  GetNow
  @comment:  get timestamp as a 64 bit int, resolution in seconds
  @return:   value as a 64 bit int
***************************************************************************/
int64_t CTimer::GetNow()
{
  std::chrono::system_clock::time_point xnow = std::chrono::system_clock::now();
  return static_cast<int64_t>(std::chrono::system_clock::to_time_t(xnow));
}

/***********************************************************************//**
  @method :  GetNow
  @comment:  get timestamp of 'now'
  @param  :  pYear    1900 -...
  @param  :  pMonth   1..12 (see enum)
  @param  :  pDay     1..31
  @param  :  pHour    0..23
  @param  :  pMinute  0..59
  @param  :  pSecond  0..59
***************************************************************************/
void CTimer::GetNow(int32_t& pYear,
                    EUCSIMonth& pMonth,
                    int32_t& pDay,
                    int32_t& pHour,
                    int32_t& pMinute,
                    int32_t& pSecond)
{
  std::chrono::system_clock::time_point xnow = std::chrono::system_clock::now();
  time_t now =std::chrono::system_clock::to_time_t(xnow);
  struct tm* pTime = localtime(&now);
  if (pTime) {
    pSecond = pTime->tm_sec;
    pMinute = pTime->tm_min;
    pHour   = pTime->tm_hour;
    pDay    = pTime->tm_mday;
    pMonth  = static_cast<EUCSIMonth>(pTime->tm_mon+1);
    pYear   = pTime->tm_year+1900;
  }
}

/***********************************************************************//**
  @method :  GetNow
  @comment:  get timestamp 'now'
  @param  :  pDay     0..6 (see enum)
  @param  :  pHour    0..23
  @param  :  pMinute  0..59
  @param  :  pSecond  0..59
***************************************************************************/
void CTimer::GetNow(EUCSIDay& pDay,
                    int32_t& pHour,
                    int32_t& pMinute,
                    int32_t& pSecond)
{
  std::chrono::system_clock::time_point xnow = std::chrono::system_clock::now();
  time_t now =std::chrono::system_clock::to_time_t(xnow);
  struct tm* pTime = localtime(&now);
  if (pTime) {
    pSecond = pTime->tm_sec;
    pMinute = pTime->tm_min;
    pHour   = pTime->tm_hour;
    pDay    = static_cast<EUCSIDay>(pTime->tm_wday);
  }
}

/***********************************************************************//**
  @method :  GetTimeStamp
  @comment:  get timestamp of given time
  @param  :  pYear    1970 -...
  @param  :  pMonth   1..12 (see enum)
  @param  :  pDay     1..31
  @param  :  pHour    0..23
  @param  :  pMinute  0..59
  @param  :  pSecond  0..59
  @return :  time in seconds since 1.1.1970
***************************************************************************/
int64_t CTimer::GetTimeStamp(int32_t pYear,
                             EUCSIMonth pMonth,
                             int32_t pDay,
                             int32_t pHour,
                             int32_t pMinute,
                             int32_t pSecond)
{
  struct tm pTime;
  pTime.tm_sec = pSecond;
  pTime.tm_min = pMinute;
  pTime.tm_hour = pHour;
  pTime.tm_mday = pDay;
  pTime.tm_mon = static_cast<int32_t>(pMonth) -1;
  pTime.tm_year = pYear -1900;

  time_t output = mktime (&pTime);
  return static_cast<int64_t> (output);
}

/***********************************************************************//**
  @method :  CTimer
  @comment:  constructor
***************************************************************************/
CTimer::CTimer()
: m_tStartTime{0}
, m_tStopTime{0}
, m_bIsReset{false}
, m_bIsRunning{false}
{
}

/***********************************************************************//**
  @method :  ~CTimer
  @comment:  destructor
***************************************************************************/
CTimer::~CTimer()
{
}

/***********************************************************************//**
  @method :  Start
  @comment:  start timer
***************************************************************************/
void CTimer::Start()
{
  if(!m_bIsRunning){
    if(m_bIsReset){
      ::time(&m_tStartTime);
    }else {
      time_t now = time(&now);
      time_t tmp = static_cast<time_t>(difftime(m_tStopTime, now));
      m_tStartTime = static_cast<time_t>(difftime(m_tStartTime, tmp));
    }
    m_bIsRunning = true;
    m_bIsReset = false;
  }
}

/***********************************************************************//**
  @method :  Stop
  @comment:  stop timer
***************************************************************************/
void CTimer::Stop()
{
  if(m_bIsRunning){
    m_bIsRunning = false;
    time(&m_tStopTime);
  }
}

/***********************************************************************//**
  @method :  Reset
  @comment:  reset timer
  @param:    restart true: restart timer
***************************************************************************/
void CTimer::Reset(bool restart)
{
  Stop();
  m_bIsReset = true;
  m_tStartTime = m_tStopTime;
  if(restart){
    Start();
  }
}

/***********************************************************************//**
  @method :  GetTime
  @comment:  get delta time start stop in seconds
             (if still running: start till now)
***************************************************************************/
int32_t CTimer::GetTime()
{
  if(m_bIsRunning) {
    time_t now = time(&now);
    return static_cast<int32_t>(difftime(now, m_tStartTime));
  } else {
    return static_cast<int32_t>(difftime(m_tStopTime, m_tStartTime));
  }
}
}
/* END_MODULE */

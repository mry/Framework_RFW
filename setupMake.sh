#!/bin/bash

# set environment
DIR=$PWD
export BASE=$DIR
export PKG_CONFIG_PATH=$DIR/DSS/lib/pkgconfig

echo "--- Framework_RFW  ---------------------------------------------------------------"
cmake  -DRFW_DEPLOY_DIR=${DIR}/deploy

make 
make install
